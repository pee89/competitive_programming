import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    f = open(os.path.abspath(__file__).replace(".py", ".in"))
    inf = f

    def input():
        return f.readline()
else:
    inf = sys.stdin

# ==============================================================


def solve():
    pass


def get_result():
    pass


n, q = [int(i) for i in input().split()]

# ==============================================================

if debug_mode:
    f.close()

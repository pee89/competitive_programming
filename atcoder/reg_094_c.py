import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def main():
    a, b, c = list(map(int, input().strip().split()))
    s = sum((a, b, c))
    x = max(a, b, c)
    steps = 0
    while True:
        if s + steps * 2 == 3 * x:
            break
        elif s + steps * 2 < 3 * x:
            steps += 1
        elif s + steps * 2 > 3 * x:
            x += 1
    print(steps)


main()

# ==============================================================

if debug_mode:
    infile.close()

from collections import namedtuple, defaultdict


def solve(cipher):
    pass


if __name__ == '__main__':
    testcases = input()

    for case_nr in range(1, testcases + 1):
        cipher = raw_input()
        print('Case #{}: {}'.format(case_nr, solve(cipher)))

import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================

from heapq import heappush, heappop

INF = 1 << 60


# dijkstra
def dijkstra(G, s):
    D = {i: INF for i in range(1, len(G) + 1)}
    D[s] = 0
    P, Q, S = {}, [(0, s)], set()
    while Q:
        _, u = heappop(Q)
        if u in S: continue
        S.add(u)
        for v in G[u]:
            d = D[u] + G[u][v]
            if d < D[v]:
                D[v], P[v] = d, u
            heappush(Q, (D[v], v))
    return D, P


def main():
    n, m = list(map(int, input().strip().split()))
    G = {i: {} for i in range(1, n + 1)}
    for _ in range(m):
        a, b, w = list(map(int, input().strip().split()))
        G[a][b], G[b][a] = w, w

    D, P = dijkstra(G, 1)

    if D[n] == INF:
        print(-1)
        return

    # construct the path
    path = [n]
    u = n
    while u != 1:
        path.append(P[u])
        u = P[u]
    print(" ".join(str(i) for i in path[::-1]))


main()

# ==============================================================

if debug_mode:
    inf.close()

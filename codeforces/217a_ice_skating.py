import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from collections import defaultdict, deque


def main():
    n = int(input().strip())
    G = defaultdict(list)
    X, Y = "x", "y"
    for _ in range(n):
        xi, yi = list(map(int, input().strip().split()))
        G[(X, xi)].append((Y, yi))
        G[(Y, yi)].append((X, xi))

    # find the number of connected components
    Q, cnt, S = deque(), 0, set()  # to visit, component count, visited
    for u in G:
        if u in S:
            continue
        cnt += 1
        Q.append((u, cnt))
        while Q:
            curr, cnt = Q.popleft()
            if curr in S:
                continue
            S.add(curr)
            for v in G[curr]:
                Q.append((v, cnt))

    print(cnt - 1)


main()

# ==============================================================

if debug_mode:
    infile.close()

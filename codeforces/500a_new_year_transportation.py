import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================

from collections import deque


def main():
    n, t = list(map(int, input().strip().split()))
    A = list(map(int, input().strip().split()))

    # construct the graph
    G = {i: [] for i in range(1, n + 1)}
    for i, a in enumerate(A):
        G[i + 1].append(i + 1 + a)

    # do bfs from 1 and see if we can reach t
    Q, S = deque([1]), set()
    while Q:
        curr = Q.popleft()
        if curr in S:
            continue
        if curr == t:
            print("YES")
            return
        Q.extend(G[curr])

    print("NO")


main()

# ==============================================================

if debug_mode:
    infile.close()

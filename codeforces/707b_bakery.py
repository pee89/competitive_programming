import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n, m, k = list(map(int, input().strip().split()))

    if k > 0:
        roads = [list(map(int, input().strip().split())) for _ in range(m)]
        storages = set(map(int, input().strip().split()))
        probable_road_lengths = list(
            l for u, v, l in roads if (u in storages) != (v in storages))
        if probable_road_lengths:
            print(min(probable_road_lengths))
        else:
            print(-1)

    else:
        print(-1)


main()

# ==============================================================

if debug_mode:
    inf.close()
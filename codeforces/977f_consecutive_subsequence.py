import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================

from collections import defaultdict


def main():
    _n = int(input().strip())
    A = list(map(int, input().strip().split()))

    d = defaultdict(int)  # d[a] length of ss ending at a
    for a in A:
        d[a] = d[a - 1] + 1

    last_num = max(d, key=lambda k: d[k])
    print(d[last_num])

    res = []
    curr = last_num - d[last_num] + 1
    for i, a in enumerate(A):
        if a == curr:
            res.append(i + 1)
            curr += 1
    print(" ".join(str(x) for x in res))


main()

# ==============================================================

if debug_mode:
    inf.close()
//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    buffer: Vec<String>,
}

impl Scanner {
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn main() {
    let mut scan = Scanner::default();
    let out = &mut BufWriter::new(io::stdout());

    // start solution

    let n: usize = scan.next();
    let a: Vec<i64> = (0..n).map(|_| scan.next()).collect();
    let b: Vec<i64> = (0..n).map(|_| scan.next()).collect();

    let mut c: Vec<_> = a
        .into_iter()
        .zip(b.into_iter())
        .map(|(ai, bi)| ai - bi)
        .collect();
    c.sort();

    let mut result: u64 = 0;
    for (i, &ci) in c.iter().enumerate() {
        let target = -ci + 1;
        let j = lower_bound(&c[i + 1..n], &target);
        let m = n - (i + 1);
        result += (m - j) as u64;
    }

    writeln!(out, "{}", result).ok();
}

use std::cmp::Ordering;

fn lower_bound(arr: &[i64], x: &i64) -> usize {
    let mut low = 0;
    let mut high = arr.len();
    while low != high {
        let mid = (low + high) / 2;
        match arr[mid].cmp(x) {
            Ordering::Less => {
                low = mid + 1;
            }
            Ordering::Equal | Ordering::Greater => {
                high = mid;
            }
        }
    }
    low
}

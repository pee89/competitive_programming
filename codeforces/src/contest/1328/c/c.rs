//! Generic utility for reading data from standard input, based on [voxl's
//! stdin wrapper](http://codeforces.com/contest/702/submission/19589375).
#![allow(unused_imports)]
use std::cmp::{max, min};
use std::io::{self, BufWriter, Write};

#[derive(Default)]
struct Scanner {
    buffer: Vec<String>,
}

impl Scanner {
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed parse");
            }
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

enum Greater {
    A,
    NA,
}

use std::char;

fn solve_case<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let n: usize = scan.next();
    let xc: Vec<char> = scan.next::<String>().chars().collect();
    let mut a: Vec<char> = vec!['-'; n];
    let mut b: Vec<char> = vec!['-'; n];
    a[0] = '1';
    b[0] = '1';
    let mut greater = Greater::NA;
    for i in 1..n {
        match xc[i] {
            '0' => {
                a[i] = '0';
                b[i] = '0';
            }
            '1' => match greater {
                Greater::NA => {
                    a[i] = '1';
                    b[i] = '0';
                    greater = Greater::A;
                }
                Greater::A => {
                    a[i] = '0';
                    b[i] = '1';
                }
            },
            '2' => match greater {
                Greater::NA => {
                    a[i] = '1';
                    b[i] = '1';
                }
                Greater::A => {
                    a[i] = '0';
                    b[i] = '2';
                }
            },
            _ => unreachable!(),
        }
    }
    let astr: String = a.iter().collect();
    let bstr: String = b.iter().collect();
    writeln!(out, "{}", astr).ok();
    writeln!(out, "{}", bstr).ok();
}

fn solve<T: Write>(scan: &mut Scanner, out: &mut BufWriter<T>) {
    let t: usize = scan.next();

    for _ in 0..t {
        solve_case(scan, out);
    }
}

fn main() {
    let mut scan = Scanner::default();
    let mut out = BufWriter::new(io::stdout());
    solve(&mut scan, &mut out);
}

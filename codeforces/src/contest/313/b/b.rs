// START =====================================================

// Problem - https://codeforces.com/problemset/problem/313/B

use std::io::{self, BufRead};

fn main() {
    // stdin
    let stdin = io::stdin();
    let guard = stdin.lock();
    let input = io::BufReader::new(guard);

    let mut lines = input.lines();

    let s: String = lines.next().unwrap().unwrap();
    let ss: Vec<char> = s.chars().collect();
    let m: i32 = lines.next().unwrap().unwrap().parse().unwrap();

    let sol = Solution::new(&ss);

    for _ in 0..m {
        let items: Vec<usize> = lines
            .next()
            .unwrap()
            .unwrap()
            .split_whitespace()
            .map(|i| i.parse::<usize>().unwrap())
            .collect();

        let l = items[0];
        let r = items[1];

        let result = sol.solve(l - 1, r - 1);
        println!("{}", result);
    }
}

#[derive(Debug)]
struct Solution {
    a: Vec<usize>,
    sum: Vec<usize>,
}

impl Solution {
    fn new(ss: &Vec<char>) -> Self {
        let n = ss.len();

        let mut a = vec![0; n];
        for i in 0..n - 1 {
            if i < n - 1 && ss[i] == ss[i + 1] {
                a[i] = 1;
            }
        }

        // upto i (inclusive)
        let mut sum = vec![0; n];
        for i in 0..n {
            sum[i] = a[i] + if i == 0 { 0 } else { sum[i - 1] };
        }

        Self { a, sum }
    }

    fn solve(&self, l: usize, r: usize) -> usize {
        let start = if l == 0 { 0 } else { self.sum[l - 1] };
        let end = self.sum[r - 1];
        end - start
    }
}

// END =====================================================

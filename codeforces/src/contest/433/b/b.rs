// Problem: https://codeforces.com/problemset/problem/433/B

use std::io::{self, BufRead, BufWriter, Write};

fn main() {
    // stddin
    let stdin = io::stdin();
    let input = io::BufReader::new(stdin);
    let mut lines = input.lines();

    // stdout
    let out = &mut BufWriter::new(io::stdout());

    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut v: Vec<u64> = lines
        .next()
        .unwrap()
        .unwrap()
        .split_whitespace()
        .map(|i| i.parse::<u64>().unwrap())
        .collect();

    let mut sum_cost = vec![0; n];
    for i in 0..n {
        sum_cost[i] = v[i] + if i == 0 { 0 } else { sum_cost[i - 1] };
    }

    v.sort();
    let mut sum_sorted_cost = vec![0; n];
    for i in 0..n {
        sum_sorted_cost[i] = v[i] + if i == 0 { 0 } else { sum_sorted_cost[i - 1] };
    }

    let m: u64 = lines.next().unwrap().unwrap().parse().unwrap();
    for _ in 0..m {
        let items: Vec<usize> = lines
            .next()
            .unwrap()
            .unwrap()
            .split_whitespace()
            .map(|i| i.parse::<usize>().unwrap())
            .collect();
        let t = items[0];
        let l = items[1] - 1;
        let r = items[2] - 1;
        let result = match t {
            1 => {
                let start = if l == 0 { 0 } else { sum_cost[l - 1] };
                sum_cost[r] - start
            }
            2 => {
                let start = if l == 0 { 0 } else { sum_sorted_cost[l - 1] };
                sum_sorted_cost[r] - start
            }
            _ => unreachable!(),
        };
        writeln!(out, "{}", result).ok();
    }
}

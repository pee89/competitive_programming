# --------------------------------------------------------------
# Default construct to add the grandparent folder to python path
# --------------------------------------------------------------
if __name__ == '__main__' and __package__ is None:
	try:
		sys
	except:
		import sys

	try:
		path
	except:
		from os import path

	parentDir = path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
	if parentDir not in sys.path:
		sys.path.append(parentDir)	
# --------------------------------------------------------------
# Default construct end
# --------------------------------------------------------------

from collections import namedtuple
from operator import itemgetter

MAX_INT = 100000

def convertToMinutes(timesList, taTime):
	"""
	List to be of the form [['HH:MM','HH:MM'],['HH:MM','HH:MM'],['HH:MM','HH:MM']]
	"""
	convertedList = []
	for times in timesList:
		convertedTimes = []
		for time in times:
			hoursAndMinutes = time.split(':')
			hours = int(hoursAndMinutes[0])
			minutes = int(hoursAndMinutes[1])
			timeInMinutes = hours * 60 + minutes
			if times.index(time) == 1:
				timeInMinutes += taTime
			convertedTimes.append(timeInMinutes)
		convertedList.append(convertedTimes)
	return convertedList

def getNextTravel(aReachingTime, AB):
	for x in xrange(len(AB)):
		currTimes = AB[x]
		if currTimes[0] >= aReachingTime:
			return AB.pop(x)

def traceNextTrain(AB, BA):
	nextTrainStart = ''

	minAStart = MAX_INT
	if len(AB) > 0:
		minAStart = AB[0][0]

	minBStart = MAX_INT
	if len(BA) > 0:
		minBStart = BA[0][0]

	if minAStart < minBStart:
		nextTrainStart = 'A'
		abTravel = AB.pop(0)
		moreTravelPossible = True
		while moreTravelPossible:	
			bReachingTime = abTravel[1]
			baTravel = getNextTravel(bReachingTime, BA)
			if baTravel:
				aReachingTime = baTravel[1]
				abTravel = getNextTravel(aReachingTime, AB)
				if not abTravel:
					moreTravelPossible = False
			else:
				moreTravelPossible = False
	else:
		nextTrainStart = 'B'
		baTravel = BA.pop(0)
		moreTravelPossible = True
		while moreTravelPossible:
			aReachingTime = baTravel[1]
			abTravel = getNextTravel(aReachingTime, AB)
			if abTravel:
				bReachingTime = abTravel[1]
				baTravel = getNextTravel(bReachingTime, BA)
				if not baTravel:
					moreTravelPossible = False
			else:
				moreTravelPossible = False

	return nextTrainStart


def main():
	filename = 'B-large-practice.in'

	with open(filename, 'r') as inFile:
		lines = inFile.readlines()
		cleanedLines = []
		for line in lines:
			if '\n' in line:
				cleanedLines.append(line[:-1])

		nCases = int(cleanedLines[0])
		print 'number of cases > {}'.format(nCases)

		# TODO > Start here
		Case = namedtuple('Case', ['taTime', 'AB', 'BA'])

		cases = []
		rowIndex = 1
		for x in xrange(nCases):
			taTime = int(cleanedLines[rowIndex])
			print 'Turanround time > {}'.format(taTime)

			allTripsString = cleanedLines[rowIndex+1]
			nAB = int(allTripsString.split()[0])
			nBA = int(allTripsString.split()[1])
			print 'Number of trips from A to B > {}'.format(nAB)
			print 'Number of trips from B to A > {}'.format(nBA)

			AB = []
			for i in xrange(rowIndex+1+1, rowIndex+1+1+nAB):
				times = cleanedLines[i].split()
				AB.append(times)
			print 'AB times > ', AB

			BA = []
			for j in xrange(rowIndex+1+1+nAB,rowIndex+1+1+nAB+nBA):
				times = cleanedLines[j].split()
				BA.append(times)
			print 'BA times > ', BA

			currCase = Case(taTime, AB, BA)
			cases.append(currCase)

			rowIndex = rowIndex + 1 + nAB + nBA + 1

		
		resultList = []
		caseNo = 1
		for currCase in cases:
			taTime = currCase.taTime

			# convert the case time string to minutes	
			# and add the turaround time to the end time
			AB = currCase.AB
			AB = convertToMinutes(AB, taTime)
			AB.sort(key=itemgetter(0))
			print 'AB > ', AB

			BA = currCase.BA
			BA = convertToMinutes(BA, taTime)
			BA.sort(key=itemgetter(0))
			print 'BA > ', BA

			nTA = 0
			nTB = 0
			while len(AB) > 0 or len(BA) > 0:
				nextTrainStart = traceNextTrain(AB,BA)
				if nextTrainStart == 'A':
					nTA += 1
				elif nextTrainStart == 'B':
					nTB += 1
				
			print 'nTA > {}'.format(nTA)
			print 'nTB > {}'.format(nTB)

			resultList.append('Case #'+str(caseNo)+': '+str(nTA)+' '+str(nTB))

			caseNo += 1

		with open(filename.replace('.in','.out'),'w') as outFile:
			outFile.write('\n'.join(resultList))



if __name__ == '__main__':
	main()
def parseString(theString):
	"""
	theString contains string in the form (ab)(bc)(ca)
	"""
	inGrp = False
	listOfGrps = []
	grp = []
	for ch in theString:
		if ch == '(':
			inGrp = True
			grp = []
		elif ch not in ['(', ')']:
			if inGrp:
				grp.append(ch)
			else:
				# if the control reaches here then we are not inside a grp
				# neither this is the start / end of the grp
				# just add this element to the master list
				listOfGrps.append([ch])
		elif ch == ')':
			inGrp = False
			listOfGrps.append(grp)

	return listOfGrps

# print parseString('(ab)(bc)(ca)')
# print parseString('(ab)b(ca)')
# print parseString('a(b)(ca)')

def getPossibleWords(listOfGrps):
 	"""listOfGrps is of the form [['a', 'b'], ['b', 'c'], ['c', 'a']]"""
 	# print listOfGrps
 	if len(listOfGrps) == 1:
 		return listOfGrps[0]
 	else:
 		subWords = getPossibleWords(listOfGrps[1:])
 		# print 'subWords >> ',subWords
 		allWords = []
 		for subWord in subWords:
 			for ch in listOfGrps[0]:
 				allWords.append(ch+subWord)
 	return allWords

# print getPossibleWords(parseString('(ab)(bc)(ca)'))

def checkWord(listOfGrps, theDict):
	# print listOfGrps
	count = 0
	for word in theDict:
		for chIndex in xrange(len(word)):
			# print '{} >> {}'.format(chIndex, listOfGrps[chIndex])
			if not (word[chIndex] in listOfGrps[chIndex]):
				# print 'not matched at char {} in {} at position {} and grp {}'.format(word[chIndex], word, chIndex, listOfGrps[chIndex])
				break
			elif chIndex == len(theDict[0]) - 1:
				count += 1
				# print word

	return count

if __name__ == '__main__':
	# print getPossibleWords(parseString('(ab)(bc)(ca)'))

	fileName = 'A-large-practice.in'

	with open(fileName, 'r') as inFile:
		lines = inFile.readlines()
		cleanedLines = []
		for line in lines:
			if '\n' in line:
				cleanedLines.append(line[:-1])
		print cleanedLines

		# we now have the cleaned lines, start work
		# print cleanedLines[0].split()
		line1Parts = cleanedLines[0].split()
		nWordsInDict = int(line1Parts[1])
		nCases = int(line1Parts[2])

		# construct the dictionary
		theDict = []
		for index in xrange(1,nWordsInDict+1):
			theDict.append(cleanedLines[index])

		# print theDict
		# print len(theDict)
		print index
		
		resultList = []
		for index in xrange(nWordsInDict+1,len(cleanedLines)):
			parsedStringList = parseString(cleanedLines[index])
			count = checkWord(parsedStringList, theDict)
			case = index - nWordsInDict
			resultList.append('Case #{}: {}'.format(case, count))

		with open(fileName.replace('.in','.out'),'w') as outFile:
			outFile.write('\n'.join(resultList))






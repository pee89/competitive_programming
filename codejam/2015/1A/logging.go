package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"math/big"
	"os"
	"sort"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readFloat() float32 {
	line := mi.readLine()
	f, err := strconv.ParseFloat(line, 32)
	if err != nil {
		panic(err)
	}
	return float32(f)
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloats() []float32 {
	res := []float32{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			panic(err)
		}
		res = append(res, float32(f))
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloat64s() []float64 {
	res := []float64{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, f)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("manasa_and_stones.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		fmt.Printf("Case #%d:\n", caseNo)
		n := mi.readInt()
		// fmt.Println("n", n)
		points := []*point{}
		for i := 1; i <= n; i++ {
			xy := mi.readFloat64s()
			points = append(points, &point{xy[0], xy[1]})
		}
		solve(n, points)
	}
}

func solve(n int, points []*point) {
	// fmt.Println("points", strPoints(points))
	if n == 1 {
		fmt.Println(0)
		return
	}

	// to determine the min number of trees to cut down for a point P,
	// for each point P, try each other point as a candiate of the next point
	// around the boundary, Q; then check how many points lie on the left of the
	// line PQ. The size of the smallest of those sets of points is the best
	// answer
	for p := 0; p < n; p++ {
		minTreeToCut := math.MaxInt32
		for q := 0; q < n; q++ {
			countForThisQ := 0
			if p == q {
				continue
			}
			for i := 0; i < n; i++ {
				if i == p || i == q {
					continue
				}
				side := findSide(points[p], points[q], points[i])
				if side == -1 {
					countForThisQ += 1
					// fmt.Println("remove pt", points[i])
				}
			}
			minTreeToCut = minOf(minTreeToCut, countForThisQ)
		}
		fmt.Println(minTreeToCut)
	}
}

type point struct{ x, y float64 }

// for sorting points by x and y
type byXY []*point

func (points byXY) Len() int { return len(points) }
func (points byXY) Less(i, j int) bool {
	if points[i].x == points[j].x {
		return points[i].y < points[j].y
	}
	return points[i].x < points[j].x
}
func (points byXY) Swap(i, j int) {
	points[i], points[j] = points[j], points[i]
}

// returns
// -1 if p is to left (counterclockwise)
// 0 if p is in line with start-end
// 1 if p is to right (clockwise)
func findSide(start, end, p *point) int {
	// p is to left of start-end if slope(end-p) > slope(start-end)
	// slopeStartEnd = (y2-y1) / (x2-x1)
	// slopeEndPoint = (y3-y2) / (x3-x2)
	v := (end.y-start.y)*(p.x-end.x) - (p.y-end.y)*(end.x-start.x)
	switch {
	case v == 0:
		return 0
	case v < 0:
		return -1
	case v > 0:
		return 1
	}

	return int(v)
}

func strPoints(points []*point) string {
	s := []string{}
	for _, p := range points {
		s = append(s, fmt.Sprintf("%v", p))
	}
	return "[" + strings.Join(s, ", ") + "]"
}

// Uses the Monotone Chain Convex Hull algorithm to find the convex hull for
// the set of points provided
func convexHull(points []*point) []*point {
	// fmt.Println("points >>")
	// fmt.Println(strPoints(points))
	n := len(points)

	// there must be atleast 3 points
	if n <= 3 {
		return points
	}

	sort.Sort(byXY(points))
	// fmt.Println("sorted >>")
	// fmt.Println(strPoints(points))

	uHull := []*point{points[0], points[1]} // upper hull
	for i := 2; i < len(points); i++ {
		// fmt.Println("considering", points[i])
		uHull = append(uHull, points[i])
		// fmt.Println("uHull", strPoints(uHull))
		for len(uHull) >= 3 {
			side := findSide(uHull[len(uHull)-3], uHull[len(uHull)-2], uHull[len(uHull)-1])
			// fmt.Println("side", side)
			if side == -1 {
				// fmt.Println("removing", uHull[len(uHull)-2])
				uHull[len(uHull)-2] = uHull[len(uHull)-1]
				uHull[len(uHull)-1] = nil // or the zero value of T
				uHull = uHull[:len(uHull)-1]
			} else {
				break
			}
		}
	}
	// fmt.Println("Upper Hull >>")
	// fmt.Println(strPoints(uHull))

	lHull := []*point{points[len(points)-1], points[len(points)-2]} // lower hull
	for i := len(points) - 3; i >= 0; i-- {
		// fmt.Println("considering", points[i])
		lHull = append(lHull, points[i])
		// fmt.Println("lHull", strPoints(lHull))
		for len(lHull) >= 3 {
			side := findSide(lHull[len(lHull)-3], lHull[len(lHull)-2], lHull[len(lHull)-1])
			// fmt.Println("side", side)
			if side == -1 {
				// fmt.Println("removing", lHull[len(lHull)-2])
				lHull[len(lHull)-2] = lHull[len(lHull)-1]
				lHull[len(lHull)-1] = nil // or the zero value of T
				lHull = lHull[:len(lHull)-1]
			} else {
				break
			}
		}
	}
	// fmt.Println("Lower Hull")
	// fmt.Println(strPoints(lHull))

	return append(uHull, lHull[1:len(lHull)-1]...)
}

func containsPoint(points []*point, p *point) bool {
	for _, pt := range points {
		if pt.x == p.x && pt.y == p.y {
			return true
		}
	}
	return false
}

func distance(p1, p2 *point) float64 {
	return math.Sqrt(math.Pow(p2.x-p1.x, float64(2)) + math.Pow(p2.y-p1.y, 2))
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"math/big"
	"os"
	"sort"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readFloat() float32 {
	line := mi.readLine()
	f, err := strconv.ParseFloat(line, 32)
	if err != nil {
		panic(err)
	}
	return float32(f)
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloats() []float32 {
	res := []float32{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			panic(err)
		}
		res = append(res, float32(f))
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloat64s() []float64 {
	res := []float64{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, f)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("manasa_and_stones.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		n := mi.readInt()
		points := []*point{}
		for i := 0; i < n; i++ {
			xy := mi.readFloat64s()
			points = append(points, &point{xy[0], xy[1]})
		}
		fmt.Printf("Case #%d:\n", caseNo)
		solve(n, points)
	}
}

type point struct{ x, y float64 }

type pointAndAngle struct {
	p   *point
	ang float64
}

type pointAndAngles []pointAndAngle

func (pa pointAndAngles) Less(i, j int) bool { return pa[i].ang < pa[j].ang }
func (pa pointAndAngles) Len() int           { return len(pa) }
func (pa pointAndAngles) Swap(i, j int)      { pa[i], pa[j] = pa[j], pa[i] }

const (
	eps    = float64(0.000001)
	deg180 = float64(180)
	zero   = float64(0)
)

func solve(n int, points []*point) {
	if n <= 3 {
		fmt.Println(0)
		return
	}

	for p := 0; p < n; p++ {
		ptA := []pointAndAngle{}
		// calculate angles for all points with center at p
		// fmt.Println("p", points[p])
		for q := 0; q < n; q++ {
			if p == q {
				continue
			}
			ang := math.Atan2(points[q].y-points[p].y, points[q].x-points[p].x)
			if cmp(ang, float64(0), eps) == -1 {
				ang += 2 * math.Pi
			}
			ang = ang * deg180 / math.Pi // pi radians = 180 deg
			ptAngle := pointAndAngle{
				points[q],
				ang,
			}
			ptA = append(ptA, ptAngle)
		}

		// sort by angles
		sort.Sort(pointAndAngles(ptA))
		// fmt.Println(strPointAndAngles(ptA))

		// find the min no. of points that would need to be removed by iterating
		// over each value of q
		minCount := math.MaxInt32
		for q := 0; q < n-1; q++ {
			countForQ := 0
			// find the points are in between pq and pq + 180deg
			for r, nPts := (q+1)%(n-1), 0; nPts < n-2; r, nPts = (r+1)%(n-1), nPts+1 {
				// fmt.Println("p", points[p], "q", ptA[q].p, "r", ptA[r].p)
				if cmp(ptA[q].ang, deg180, eps) == -1 {
					// ptA[q] < 180
					if !(cmp(ptA[r].ang, ptA[q].ang, eps) == 1 &&
						cmp(ptA[r].ang, ptA[q].ang+deg180, eps) == -1) {
						break
					}
				} else {
					// ptA[q] >= 180
					if cmp(ptA[r].ang, deg180, eps) == -1 {
						delta := ptA[q].ang - deg180
						// fmt.Println("delta", delta, "ptA[r].ang", ptA[r].ang)
						if cmp(ptA[r].ang, delta, eps) != -1 {
							break
						}
					} else {
						if cmp(ptA[r].ang, ptA[q].ang, eps) == -1 {
							break
						}
					}
				}
				countForQ += 1
			}
			// fmt.Println("countForQ", countForQ)
			minCount = minOf(minCount, countForQ)
		}
		fmt.Println(minCount)
	}
}

// returns -1 if f1 is less than f2
// returns 0 if f1 is less than f2
// returns 1 if f1 is greater than f2
func cmp(f1, f2, eps float64) int {
	// fmt.Printf("comparing %f and %f\n", f1, f2)
	if f1 == f2 || math.Abs(f1-f2) < eps {
		return 0
	}
	if f1 < f2 {
		return -1
	} else {
		return 1
	}
}

func strPointAndAngles(pas []pointAndAngle) string {
	s := []string{}
	for _, pa := range pas {
		s = append(s, fmt.Sprintf("[%v, %f]", pa.p, pa.ang))
	}
	return "[" + strings.Join(s, ", ") + "]"
}

func minOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

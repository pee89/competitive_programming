package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readFloat() float32 {
	line := mi.readLine()
	f, err := strconv.ParseFloat(line, 32)
	if err != nil {
		panic(err)
	}
	return float32(f)
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readBigInt() *big.Int {
	return mi.readBigIntToBase(10)
}

func (mi *MyInput) readBigIntToBase(base int) *big.Int {
	line := mi.readLine()
	var bi big.Int
	bi.SetString(line, base)
	return &bi
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloats() []float32 {
	res := []float32{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			panic(err)
		}
		res = append(res, float32(f))
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readFloat64s() []float64 {
	res := []float64{}
	for _, s := range mi.readWords() {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, f)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("manasa_and_stones.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		n := mi.readInt64()
		fmt.Printf("Case #%d: %d\n", caseNo, solve(n))
	}
}

func str(i int64) string {
	return strconv.FormatInt(i, 10)
}

func solve(n int64) int64 {
	if n <= int64(19) {
		return n
	}

	one := int64(1)
	ten := int64(10)
	steps := int64(0)
	strN := strconv.FormatInt(n, 10)
	digits := len(strN)

	// get in the fastest way to 10, 100, 1000 etc as per the number of digits
	// in n
	tenPow := int64(math.Pow(float64(10), float64(digits-1)))
	tenPowC := tenPow
	for tenPowC > ten {
		// 19 -> 91 -> 100 (steps s(10) + 9 + 1 + 9)
		// 19 -> 91 -> 100 -> 109 -> 901 -> 1000 (steps s(100) + 9 + 1 + 99)
		// 19 -> 91 -> 100 -> 109 -> 901 -> 1000 -> 1009 -> 9001 -> 10000 (steps s(1000) + 9 + 1 + 999)
		l := len(str(tenPowC))
		steps += int64(9) + one + int64(math.Pow(float64(10), float64(l-2))) - one
		tenPowC = tenPowC / ten
	}
	steps += 10 // steps 10

	if n == tenPow {
		return steps
	}

	// once there, use the min flips to reach n
	firstDigit, _ := strconv.ParseInt(string(strN[0]), 10, 64)
	// 890 : 100 > 108 > 801 > 890
	steps += firstDigit // 100 > 108
	steps += one        // flip
	t, _ := strconv.ParseInt(str(firstDigit)+strings.Repeat("0", digits-2)+str(one), 10, 64)
	steps += n - t // 8 + 0 + 1

	return steps
}

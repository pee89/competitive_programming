package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE START ////////////////////////////////////////////////////////

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res, ok := <-mi.lineChan
	if !ok {
		panic("trying to read from a closed channel")
	}
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END //////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("manasa_and_stones.in")
	// defer f.Close()
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		n := mi.readInt()
		bfArr := mi.readInts()
		fmt.Printf("Case #%d: %d\n", caseNo, solve(n, bfArr))
		// fmt.Println("#########################")
	}
}

func solve(n int, bfArr []int) int {
	bfArr = append([]int{0}, bfArr...)
	// fmt.Println("bfArr", bfArr)
	max := 0
	circle := []int{}
	for start := 1; start <= n; start++ {
		// build the max circle
		// fmt.Println("start", start)
		circle = []int{start}
		done := false
		for !done {
			// fmt.Println("circle", circle)
			bf := bfArr[circle[len(circle)-1]]
			// fmt.Println("bf", bf)
			switch {

			case circle[0] == bf:
				// fmt.Println("circle[0] == bf, breaking now")
				// fmt.Println(circle, "case 1")
				done = true

			case len(circle) >= 2 && circle[len(circle)-2] == bf:
				// fmt.Println("inside case 2")
				if next, found := anyLeftForNearLast(bfArr, circle); found {
					// is there anyone left who wants to sit near circle[len(circle)-1]
					circle = append(circle, next)
					continue
				} else if next, found := anyLeftForNearFirst(bfArr, circle); found {
					// is there anyone left who wants to sit near circle[0]
					circle = append(circle, next)
					// fmt.Println(circle, "case 2.1")
					done = true
				} else {
					// fmt.Println(circle, "case 2.2")
					done = true
				}

			case contains(circle, bf):
				if circle[0] != bf {
					circle = circle[:len(circle)-1]
				}
				// fmt.Println(circle, "case 3")
				done = true

			default:
				circle = append(circle, bf)
				continue
			}
		}
		max = maxOf(max, len(circle))
	}
	return max
}

func contains(arr []int, v int) bool {
	for _, i := range arr {
		if i == v {
			return true
		}
	}
	return false
}

func anyLeftForNearLast(arr, circle []int) (next int, found bool) {
	// fmt.Println("anyLeftForNearLast", arr, circle)
	last := circle[len(circle)-1]
	for i := 1; i <= len(arr)-1; i++ {
		if !contains(circle, i) {
			if arr[i] == last {
				// fmt.Println(i, true)
				return i, true
			}
		}
	}
	// fmt.Println(-1, false)
	return -1, false
}

func anyLeftForNearFirst(arr, circle []int) (next int, found bool) {
	// fmt.Println("anyLeftForNearFirst", arr, circle)
	first := circle[len(circle)-1]
	for i := 1; i <= len(arr)-1; i++ {
		if !contains(circle, i) {
			if arr[i] == first {
				// fmt.Println(i, true)
				return i, true
			}
		}
	}
	// fmt.Println(-1, false)
	return -1, false
}

func maxOf(a, b int) int {
	if a < b {
		return b
	}
	return a
}

package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END

func main() {
	// f, _ := os.Open("coin_jam.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		nj := mi.readInts()
		n := nj[0]
		j := nj[1]
		fmt.Printf("Case #%d:\n", caseNo)
		solve(n, j)
	}
}

var zero = big.NewInt(0)
var one = big.NewInt(1)
var two = big.NewInt(2)

func solve(n, j int) {
	a := []string{"1"}
	for i := 0; i < n-1; i++ {
		a = append(a, "0")
	}
	s := strings.Join(a, "")
	var si2 big.Int
	si2.SetString(s, 2)

	si2.Sub(&si2, one)
	s = si2.Text(2)

	var num big.Int

	count := 0
	for count < j {
		// increment by 2
		si2.Add(&si2, two)
		s = si2.Text(2)

		nonPrimeInAllBases := true
		for base := 2; base <= 10; base++ {
			num.SetString(s, base)
			if num.ProbablyPrime(40) {
				nonPrimeInAllBases = false
				break
			}
		}

		if nonPrimeInAllBases {
			nonTrivDivs := getNonTrivDivs(&s)
			if len(nonTrivDivs) != 9 {
				continue
			}
			strNonTrivDivs := []string{}
			for _, d := range nonTrivDivs {
				strNonTrivDivs = append(strNonTrivDivs, d.String())
			}
			fmt.Printf("%s %s\n", s, strings.Join(strNonTrivDivs, " "))
			count += 1
		}

	}
}

func getNonTrivDivs(s *string) []*big.Int {
	res := []*big.Int{}
	var num big.Int
	var d big.Int
	var numHalf big.Int
	var divLimit = big.NewInt(100000)
	var mod big.Int
	for base := 2; base <= 10; base++ {
		num.SetString(*s, base)
		numHalf.Div(&num, two)
		numHalf.Add(&numHalf, one)
		for d.SetInt64(2); d.Cmp(divLimit) == -1; d.Add(&d, one) {
			if mod.Mod(&num, &d).Cmp(zero) == 0 {
				var div big.Int
				div.SetString(d.String(), 10)
				res = append(res, &div)
				break
			}
		}
	}
	return res
}

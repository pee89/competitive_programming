package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END

func main() {
	// f, _ := os.Open("fractiles.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		kcs := mi.readInts()
		k := kcs[0]
		c := kcs[1]
		s := kcs[2]
		res := solve(k, c, s)
		if len(res) == 0 {
			fmt.Printf("Case #%d: IMPOSSIBLE\n", caseNo)
		} else {
			strRes := []string{}
			for _, i := range res {
				strRes = append(strRes, strconv.Itoa(i))
			}
			fmt.Printf("Case #%d: %s\n", caseNo, strings.Join(strRes, " "))
		}
	}
}

func solve(k, c, s int) []int {
	res := []int{}

	// the leftmost k tiles are the original tiles are eithe
	// 1. the original tiles (in case the leftmost original tile was L)
	// 2. G tiles (in case the leftmost original tile was G)

	// so for the small case, the simple solution is to check
	// the first k tiles :D

	if s >= k {
		for i := 1; i <= k; i++ {
			res = append(res, i)
		}
	}

	return res
}

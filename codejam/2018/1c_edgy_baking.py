import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================

from math import sqrt

def solve(N, P, cookies):
    # perimeter of uncut cookies
    min_p = sum(c[2] for c in cookies)
    max_p = sum(c[3] for c in cookies)
    
    if P <= min_p:
        return min_p - P
    
    elif P >= max_p:
        return P - max_p

    else:
        p_balance = import pdb; pdb.set_trace()


def main():
    cases = int(input().strip())
    for case_no in range(1, cases + 1):
        N, P = list(map(int, input().strip().split()))
        cookies = []
        for _ in range(N):
            wh = list(map(int, input().strip().split()))
            wh.append(2 * (wh[0] + wh[1]))
            wh.append(wh[2] + 2 * sqrt(wh[0] * wh[0] + wh[1] * wh[1]))
            wh.sort()
            cookies.append(wh)

        result = solve(N, P, cookies)
        print("Case #{}: {}".format(case_no, result))


main()

# ==============================================================

if debug_mode:
    inf.close()
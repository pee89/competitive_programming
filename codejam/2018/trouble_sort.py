import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def solve(n, v):
    even = v[::2]
    odd = v[1::2]
    even.sort()
    odd.sort()
    final = []
    for idx in range(n // 2):
        final.append(even[idx])
        final.append(odd[idx])

    if n % 2 != 0:
        final.append(even[n // 2])

    for idx, (a, b) in enumerate(zip(final, final[1:])):
        if a > b:
            return idx

    return -1


def main():
    n_cases = int(input().strip())
    for case_no in range(1, n_cases + 1):
        n = int(input().strip())
        v = list(map(int, input().strip().split()))
        result = solve(n, v)
        if result == -1:
            print("Case #{}: {}".format(case_no, "OK"))
        else:
            print("Case #{}: {}".format(case_no, result))


main()

# ==============================================================

if debug_mode:
    inf.close()
import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


# str -> (int, int)
def solve(N):
    a, b = "", ""
    for ci in N[::-1]:
        if ci == "4":
            a += "2"
            b += "2"
        else:
            a += ci
            b += "0"
    a, b = a[::-1], b[::-1]
    a, b = int(a), int(b)
    return a, b


def main():
    T = int(input())
    for i in range(1, T + 1):
        N = input()
        a, b = solve(N)
        print("Case #{}: {} {}".format(i, a, b))


main()

# ==============================================================

if debug_mode:
    inf.close()

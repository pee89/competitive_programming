import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def count_ones(arr, rend, cend, k):
    ones = 0
    for r in range(rend - k + 1, rend + 1):
        for c in range(cend - k + 1, cend + 1):
            if arr[r][c] == 1:
                ones += 1
    return ones


def convert_to_zeros(arr, rend, cend, k):
    for r in range(rend - k + 1, rend + 1):
        for c in range(cend - k + 1, cend + 1):
            arr[r][c] = 0


def main():
    N, M, K = list(map(int, input().strip().split()))
    arr = []
    for _ in range(N):
        arr.append(list(map(int, input().strip().split())))

    max_ones, max_rend, max_cend = -1, K - 1, K - 1
    for r in range(K - 1, N):
        for c in range(K - 1, M):
            ones = count_ones(arr, r, c, K)
            if ones > max_ones:
                max_ones = ones
                max_rend = r
                max_cend = c

    convert_to_zeros(arr, max_rend, max_cend, K)

    max_ones, max_rend, max_cend = -1, K - 1, K - 1
    for r in range(K - 1, N):
        for c in range(K - 1, M):
            ones = count_ones(arr, r, c, K)
            if ones > max_ones:
                max_ones = ones
                max_rend = r
                max_cend = c

    convert_to_zeros(arr, max_rend, max_cend, K)

    zeros = 0
    for r in range(N):
        for c in range(M):
            if arr[r][c] == 0:
                zeros += 1
    print(zeros)


main()

# ==============================================================

if debug_mode:
    infile.close()

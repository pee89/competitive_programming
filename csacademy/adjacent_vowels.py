import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    s = input().strip()

    vowels = set(["a", "e", "i", "o", "u"])

    cnt = 0
    for i in range(1, n):
        if s[i] in vowels and s[i - 1] in vowels:
            cnt += 1

    print(cnt)


main()

# ==============================================================

if debug_mode:
    inf.close()
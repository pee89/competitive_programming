import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    w = list(list(input()) for _ in range(n))
    
    for i in range(n):
        w[i].sort()
        w[i] = str(w[i])

    w.sort()
    
    from collections import defaultdict
    f = defaultdict(int)
    m = {}
    for s in w:
        f[s] += 1
        m = max(m, f[s])
    
    print(m)


main()

# ==============================================================

if debug_mode:
    inf.close()

import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n, k = list(map(int, input().strip().split()))
    h = list(map(int, input().strip().split()))

    left, right = 0, max(h)
    while left < right:
        mid = (left + right + 1) // 2
        hm = [x // mid for x in h]
        if sum(hm) >= k:
            left = mid
        else:
            right = mid - 1

    print(left)


main()

# ==============================================================

if debug_mode:
    inf.close()

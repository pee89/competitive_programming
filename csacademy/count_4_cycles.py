import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    t1, t2 = set(), set()
    for _ in range(1, n):
        a, b = list(map(int, input().strip().split()))
        if a > b:
            a, b = b, a
        t1.add((a, b))
    for _ in range(1, n):
        a, b = list(map(int, input().strip().split()))
        if a > b:
            a, b = b, a
        t2.add((a, b))

    common = t1 & t2
    print(len(common))


main()

# ==============================================================

if debug_mode:
    inf.close()
import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================

c = {
    "0": 1,
    "1": 0,
    "2": 0,
    "3": 0,
    "4": 0,
    "5": 0,
    "6": 1,
    "7": 0,
    "8": 2,
    "9": 1,
}


def main():
    a, b = list(map(int, input().strip().split()))

    mhn = a
    mh = sum(c[i] for i in str(a))
    for n in range(a, b + 1):
        hc = sum(c[i] for i in str(n))
        if hc > mh:
            mh = hc
            mhn = n

    print(mhn)


main()

# ==============================================================

if debug_mode:
    inf.close()

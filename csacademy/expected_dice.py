import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    fd = list(map(int, input().split()))
    sd = list(map(int, input().split()))

    from collections import defaultdict
    d = defaultdict(int)

    for f in fd:
        for s in sd:
            d[f + s] += 1

    from operator import itemgetter
    items = list(d.items())
    items.sort()
    m = max(items, key=itemgetter(1))
    print(m[0])


main()

# ==============================================================

if debug_mode:
    inf.close()

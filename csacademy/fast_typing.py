import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    s = input().strip()
    t = list(map(int, input().strip().split()))

    d = dict(zip("abcdefghijklmnopqrstuvwxyz", t))

    tt = 0
    for ch in s:
        tt += d[ch]

    print(tt)


main()

# ==============================================================

if debug_mode:
    inf.close()

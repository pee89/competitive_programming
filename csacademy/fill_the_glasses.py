import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n, k = map(int, input().split())
    g = list(map(int, input().split()))
    g.sort()

    w = sum(g[:k])
    res = (w + 99) // 100

    print(res)


main()

# ==============================================================

if debug_mode:
    inf.close()

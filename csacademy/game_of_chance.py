import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n, a, b = list(map(int, input().strip().split()))
    alex = list(map(int, input().strip().split()))
    ben = list(map(int, input().strip().split()))

    fa = a + sum(alex)
    fb = b + sum(ben)
    if fa > fb:
        print(1)
    elif fb > fa:
        print(2)
    else:
        print(0)


main()

# ==============================================================

if debug_mode:
    inf.close()

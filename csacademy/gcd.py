def gcd(x, y):
    while y:
        x, y = y, x % y
    return x
        
        
a, b = [int(x) for x in input().strip().split()]
print(gcd(a, b))
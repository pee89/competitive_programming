import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    a = list(map(int, input().split()))

    a.sort()
    ms = 0
    for i in range(0, n, 2):
        ms += a[i+1] - a[i]

    print(ms)

main()

# ==============================================================

if debug_mode:
    inf.close()

import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def main():
    n, k = list(map(int, input().strip().split()))
    arr = [None] * n
    for _ in range(n):
        a, b = list(map(int, input().strip().split()))
        arr[b - 1] = a

    races = []
    for cl in arr:
        # use binary search to find the race to
        # append the current driver to
        lo, hi = 0, len(races)
        while lo < hi:
            mid = (lo + hi) // 2
            if cl <= races[mid][-1]:
                lo = mid + 1
            else:
                hi = mid
        if hi == len(races):
            races.append([cl])
        else:
            races[lo].append(cl)

    print(len(races))


main()

# ==============================================================

if debug_mode:
    infile.close()

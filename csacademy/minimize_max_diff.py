import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    infile = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return infile.readline()


# ==============================================================


def main():
    n, k = list(map(int, input().strip().split()))
    arr = list(map(int, input().strip().split()))

    diff = [v2 - v1 for v1, v2 in zip(arr, arr[1:])]

    # do a sliding window of size (n-1-k) over diffs to find the
    # minimum value of subarray's max value
    window = n - 1 - k

    from collections import deque
    d = deque()
    max_diffs = []

    # initial window
    for i in range(window):
        if len(d) == 0:
            d.append(diff[i])
        else:
            while len(d) > 0 and d[-1] < diff[i]:
                d.pop()
            d.append(diff[i])

    max_diffs.append(d[0])

    # slide the window over the diff
    for i in range(window, n - 1):
        if len(d) == 0:
            d.append(diff[i])
        else:
            while len(d) > 0 and d[-1] < diff[i]:
                d.pop()
            d.append(diff[i])

        if diff[i - window] == d[0]:
            d.popleft()

        max_diffs.append(d[0])

    print(min(max_diffs))


main()

# ==============================================================

if debug_mode:
    infile.close()

import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input().strip())
    s = input().strip()

    from collections import defaultdict
    d = defaultdict(int)
    for ch in s:
        d[ch] += 1

    idx = -1
    for i, ch in enumerate(s):
        if d[ch] == 1:
            idx = i
            break

    if idx == -1:
        print(-1)
    else:
        print(s[idx])


main()

# ==============================================================

if debug_mode:
    inf.close()

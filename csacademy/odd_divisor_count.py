import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

from math import floor


def main():
    a, b = [int(x) for x in input().strip().split()]
    res = 0
    for n in range(a, b + 1):
        divs = divisors(n)
        if len(divs) % 2 != 0:
            res += 1
    print(res)


from math import sqrt


def divisors(n):
    res = []
    for i in range(1, floor(sqrt(n) + 1)):
        if n % i == 0:
            if i == n / i:
                res.append(i)
            else:
                res.append(i)
                res.append(int(n / i))
    return res


main()

# ==============================================================

if debug_mode:
    inf.close()

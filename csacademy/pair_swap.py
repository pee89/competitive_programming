import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n, k = list(map(int, input().strip().split()))
    v = list(map(int, input().strip().split()))

    if k == n:
        k = n - 1

    from collections import deque
    deq = deque()

    for i in range(1, k + 1):
        while deq and v[deq[-1]] >= v[i]:
            deq.pop()
        deq.append(i)

    for i in range(n - 1):
        while deq and i + k < n and v[deq[-1]] >= v[i + k]:
            deq.pop()

        if i + k < n:
            deq.append(i + k)

        if deq[0] == i:
            deq.popleft()

        if v[i] > v[deq[0]]:
            v[i], v[deq[0]] = v[deq[0]], v[i]
            break

    print(" ".join(str(x) for x in v))


main()

# ==============================================================

if debug_mode:
    inf.close()

import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n, x = map(int, input().strip().split())
    ab = []
    for _ in range(n):
        ab.append(list(map(int, input().strip().split())))

    ab.sort()

    disc = 0
    mc = x
    for c, d in ab:
        disc += d
        if c > x:
            mc = min(mc, c - disc)
        else:
            mc = min(mc, x - disc)

    print(mc)


main()

# ==============================================================

if debug_mode:
    inf.close()

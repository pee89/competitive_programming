import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()

else:
    inf = sys.stdin

# ==============================================================


def main():
    h, w, n, m = map(int, input().strip().split())
    yy = list(map(int, input().strip().split()))
    xx = list(map(int, input().strip().split()))

    xx.insert(0, 0)
    xx.append(w)
    yy.insert(0, 0)
    yy.append(h)

    xx.sort()
    yy.sort()

    xx_diff = [t - s for s, t in zip(xx, xx[1:])]
    yy_diff = [t - s for s, t in zip(yy, yy[1:])]

    from collections import defaultdict
    yy_cnt = defaultdict(int)
    for yd in yy_diff:
        yy_cnt[yd] += 1

    cnt = sum(yy_cnt[x] for x in xx_diff)
    print(cnt)


main()

# ==============================================================

if debug_mode:
    inf.close()

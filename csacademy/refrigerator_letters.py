import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    n = int(input().strip())
    cc = list(input().strip().split())
    s = input().strip()

    from collections import defaultdict

    ds = defaultdict(int)
    for ch in s:
        ds[ch] += 1

    dcc = defaultdict(int)
    for c in cc:
        dcc[c] += 1

    cnt = 0
    for ch in ds:
        if ds[ch] > dcc[ch]:
            cnt += ds[ch] - dcc[ch]

    print(cnt)


main()

# ==============================================================

if debug_mode:
    inf.close()

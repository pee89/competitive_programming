import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    n = int(input())
    shoes = []
    for _ in range(n):
        shoes.append(input().split())

    shoes.sort()

    from operator import itemgetter
    from itertools import groupby

    buckets = []
    for _size, size_shoes in groupby(shoes, itemgetter(0)):
        lr = [record[1] for record in size_shoes]
        buckets.append(lr)

    pairs = 0
    for b in buckets:
        l = b.count("L")
        r = b.count("R")
        pairs += min(l, r)

    print(pairs)


def main2():
    n = int(input())
    n_shoes = [[0, 0] for _ in range(101)]
    for _ in range(n):
        sz, lr = input().split()
        sz = int(sz)
        if lr == "L":
            n_shoes[sz][0] += 1
        else:
            n_shoes[sz][1] += 1

    pairs = 0
    for sz_shoes in n_shoes:
        pairs += min(sz_shoes)

    print(pairs)


main2()

# ==============================================================

if debug_mode:
    inf.close()

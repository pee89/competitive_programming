import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================


def main():
    moves = input()
    n = moves.count("N") - moves.count("S")
    e = moves.count("E") - moves.count("W")
    print(abs(n) + abs(e))


main()

# ==============================================================

if debug_mode:
    inf.close()

import math
from collections import Counter


def nC2(n):
    return n * (n - 1) / 2


calculated_combinations = {}


def dist(a, b):
    return math.hypot(b[0] - a[0], b[1] - a[1])


def solve(stars):
    total_count = 0
    # iterate over each star
    for curr_star in stars:
        distances = []
        # for each star, calculate the distances of each other star from it
        for star in stars:
            if curr_star == star:
                continue
            # store the distances in a list
            distances.append(dist(curr_star, star))

        # find the frequency of each distance in the list
        freqs = Counter(distances)
        # print("freqs >>", freqs)

        # do nC2 for each distance frequency for which the frequency is
        # than two
        for d in freqs:
            if freqs[d] >= 2:
                if freqs[d] not in calculated_combinations:
                    calculated_combinations[freqs[d]] = nC2(freqs[d])
                total_count += calculated_combinations[freqs[d]]

    return int(total_count)


if __name__ == '__main__':
    n_cases = int(input().strip())
    # print(n_cases)

    for case_no in range(1, n_cases + 1):
        # print("Case No.:", case_no)

        N = int(input().strip())
        # print(N)

        stars = []
        for i in range(N):
            star = input().strip().split()
            stars.append((int(star[0]), int(star[1])))
        # print(stars)

        count = solve(stars)

        print("Case #{}: {}".format(case_no, count))

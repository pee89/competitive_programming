use std::io::stdin;
use std::f64;
use std::i32;
use std::collections::HashMap;

mod utils;
use utils::{read_int, read_line, nC2};

fn main() {
    // no of cases
    let n_cases = read_int();
    // println!("n_cases: {}", n_cases);

    for case_no in 1..n_cases + 1 {
        // println!("case no: {}", case_no);

        // no of stars
        let N = read_int();
        // println!("no. of stars: {}", N);

        // read the star positions
        let mut stars: Vec<(i64, i64)> = Vec::new();
        for i in 0..N {
            let s = read_line();
            let pos: Vec<i64> = s.split(' ')
                                 .map(|x| x.parse().unwrap())
                                 .collect();
            let pos_tuple: (i64, i64) = (pos[0], pos[1]);
            stars.push(pos_tuple);
        }
        // println!("{:?}", stars);

        // for each star, find the distance of each other star from it
        let mut total_count = 0;
        for star in &stars {

            let mut dist: Vec<f64> = Vec::new();

            for other_star in &stars {
                if star == other_star {
                    continue;
                }


                let xdist = (other_star.0 - star.0).abs() as f64;
                let ydist = (other_star.1 - star.1).abs() as f64;
                // println!("{}, {}", xdist, ydist);
                dist.push(xdist.hypot(ydist));
            }

            // now we have the distances of each other star from this star
            // find the freq of the distances
            let mut freq: HashMap<String, u64> = dist_freq_hashmap(&dist);
            // println!("{:?}", freq);

            // for distances that have a freq more than 2, do nC2 and sum them all up
            for (d, count) in &freq {
                if *count >= 2u64 {
                    let d_f64 = d.parse::<f64>().unwrap();
                    total_count += utils::nC2(*count);
                }
            }
        }
        println!("Case #{}: {}", case_no, total_count);
    }
}

fn dist_freq_hashmap(data: &Vec<f64>) -> HashMap<String, u64> {
    let mut res: HashMap<String, u64> = HashMap::new();

    for d in data.iter() {
        let count = res.entry(d.to_string()).or_insert(0);
        *count += 1;
    }

    res
}

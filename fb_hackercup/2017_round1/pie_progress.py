import sys
import itertools
from math import inf as INF


def solve(n, m, cost):
    # sort each day's row
    for i in range(n):
        cost[i].sort()

    # replace each cij with the value considering adjustment for p
    for i in range(n):
        p = 0
        for j in range(m):
            p += 1
            cost[i][j] += p * p - (p - 1) * (p - 1)

    result = 0
    for i in range(n):
        min = INF
        min_row = -1
        for i, r in enumerate(cost):
            if r and r[0] < min:
                min = r[0]
                min_row = i
        result += cost[min_row].pop(0)

    return result


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        import os
        f = open(os.path.abspath(__file__).replace(".py", ".txt"))

        def input():
            return f.readline()

    t = int(input())
    for caseno in range(1, t + 1):
        n, m = map(int, input().strip().split())
        cost = []
        for lineno in range(n):
            cost.append(list(map(int, input().strip().split())))
        result = solve(n, m, cost)
        print("Case #{}: {}".format(caseno, result))

    if len(sys.argv) > 1 and sys.argv[1] == "-d":
        f.close()

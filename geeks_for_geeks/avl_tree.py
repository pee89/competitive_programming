# AVL Tree


class Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None
        self.height = 1


class AVLTree:

    # Node, any -> Node
    @staticmethod
    def insert(root, val):

        # 1. insert the value
        if not root: return Node(val)
        elif val < root.val: root.left = AVLTree.insert(root.left, val)
        elif val > root.val: root.right = AVLTree.insert(root.right, val)

        # 2. update the height of the node
        root.height = 1 + max(
            AVLTree.getHeight(root.left), AVLTree.getHeight(root.right))

        # 3. check that the node is balanced
        balance = AVLTree.getHeight(root.left) - AVLTree.getHeight(root.right)

        # if the node is unbalanced, rotate it appropriately
        # 3.1 left left case => rotate right @ root
        if balance > 1 and val < root.left.val:
            root = AVLTree.rightRotate(root)

        # 3.2 left right case => rotate left @ root.left then rotate right @ root
        if balance > 1 and val > root.left.val:
            root.left = AVLTree.leftRotate(root.left)
            root = AVLTree.rightRotate(root)

        # 3.3 right right case => rotate left @ root
        if balance < -1 and val > root.right.val:
            root = AVLTree.leftRotate(root)

        # 3.4 right left case => rotate right @ root.right then rotate left @ root
        if balance < -1 and val < root.right.val:
            root.right = AVLTree.rightRotate(root.right)
            root = AVLTree.leftRotate(root)

        # 4. return the updated node
        return root

    # Node -> int
    @staticmethod
    def getBalance(root):
        if not root: return 0
        else: return getHeight(root.left) - getHeight(root.right)

    # Node -> int
    @staticmethod
    def getHeight(root):
        if not root: return 0
        else: return root.height

    # Node -> Node
    @staticmethod
    def leftRotate(z):
        #     z               y
        #    / \             / \
        #   T1  y     =>    z   T3
        #      / \         / \
        #     T2  T3     T1   T2

        # 1. rotate
        y = z.right
        T2 = y.left
        z.right = T2
        y.left = z

        # 2. udpate heights
        z.height = 1 + max(
            AVLTree.getHeight(z.left), AVLTree.getHeight(z.right))
        y.height = 1 + max(
            AVLTree.getHeight(y.left), AVLTree.getHeight(y.right))

        return y

    # Node -> Node
    @staticmethod
    def rightRotate(z):
        #       z               y
        #      / \             / \
        #     y   T3   =>    T1   z
        #    / \                 / \
        #  T1  T2              T2   T3

        # 1. rotate
        y = z.left
        T2 = y.right
        z.left = T2
        y.right = z

        # 2. update heights
        z.height = 1 + max(
            AVLTree.getHeight(z.left), AVLTree.getHeight(z.right))
        y.height = 1 + max(
            AVLTree.getHeight(y.left), AVLTree.getHeight(y.right))

        return y

    @staticmethod
    def repr(root):
        if not root: return None
        else:
            leftRepr = AVLTree.repr(root.left)
            rightRepr = AVLTree.repr(root.right)
            return f"[val: {root.val}, left: {leftRepr}, right: {rightRepr}]"


# TEST
if __name__ == "__main__":
    tree = Node(1)
    tree = AVLTree.insert(tree, 2)
    tree = AVLTree.insert(tree, 3)
    tree = AVLTree.insert(tree, 4)
    tree = AVLTree.insert(tree, 5)
    tree = AVLTree.insert(tree, 6)
    print(AVLTree.repr(tree))
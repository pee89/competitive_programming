from typing import Dict, Set, List, TypeVar
from collections import defaultdict

# types
Node = TypeVar("Node")
Graph = Dict[Node, Set[Node]]


def walk(G: Graph, u: Node, S: Set[Node] = set()) -> Set[Node]:
    seen = set(S)
    todo = set([u])
    comp = set()
    while todo:
        curr = todo.pop()
        if curr in seen:
            continue
        seen.add(curr)
        comp.add(curr)
        todo.update(G[curr])
    return comp


def test_walk():
    print("test walk:")
    graph = defaultdict(set)
    a, b, c, d, e = range(5)
    graph[a].add(b)
    graph[b].add(c)
    graph[c].add(a)
    graph[d].add(e)
    graph[e]
    print("graph: {}".format(graph))
    result = walk(graph, a)
    print("result: {}".format(result))
    assert set([a, b, c]) == result


def dfs_topsort(G: Graph) -> List[Node]:
    discovered: Set[Node] = set()
    finished: List[Node] = []

    def dfs(u: Node) -> None:
        if u in discovered:
            return
        discovered.add(u)
        for v in G[u]:
            dfs(v)
        finished.append(u)

    for u in G:
        dfs(u)

    finished.reverse()
    return finished


def test_dfs_topsort():
    print("test_dfs_topsort:")
    graph = defaultdict(set)
    a, b, c, d, e, f, g = range(7)
    graph[a].add(b)
    graph[b].add(c)
    graph[c].add(a)
    graph[c].add(d)
    graph[d].add(e)
    graph[e].add(f)
    graph[f].add(d)
    graph[f].add(g)
    graph[g]
    print("graph: {}".format(graph))

    result = dfs_topsort(graph)
    print("result: {}".format(result))

    def idx(val):
        return result.index(val)

    assert max(map(idx, [a, b, c])) < min(map(idx, [d, e, f]))
    assert max(map(idx, [d, e, f])) < idx(g)


def transpose(G: Graph) -> Graph:
    GT: Graph = defaultdict(set)
    for u in G:
        for v in G[u]:
            GT[v].add(u)
    return GT


def scc(G: Graph) -> List[Set[Node]]:
    GT: Graph = transpose(G)
    seen: Set[Node] = set()
    sccs: List[Set[Node]] = []
    for u in dfs_topsort(G):
        if u in seen:
            continue
        C: Set[Node] = walk(GT, u, seen)
        seen.update(C)
        sccs.append(C)
    return sccs


def test_scc():
    print("test_dfs_topsort:")
    graph = defaultdict(set)
    a, b, c, d, e, f, g = range(7)
    graph[a].add(b)
    graph[b].add(c)
    graph[c].add(a)
    graph[c].add(d)
    graph[d].add(e)
    graph[e].add(f)
    graph[f].add(d)
    graph[f].add(g)
    graph[g]
    print("graph: {}".format(graph))

    sccs = scc(graph)
    print("sccs: {}".format(sccs))

    assert len(sccs) == 3
    assert set([a, b, c]) in sccs
    assert set([d, e, f]) in sccs
    assert set([g]) in sccs


def main() -> None:
    n, m = [int(x) for x in input().strip().split()]
    graph: Graph = defaultdict(set)
    for _ in range(m):
        a, b = [int(x) for x in input().strip().split()]
        graph[a].add(b)
        graph[b]

    sccs: List[Set[int]] = scc(graph)

    C = sum(len(scc) for scc in sccs if len(scc) % 2 != 0)
    D = sum(len(scc) for scc in sccs if len(scc) % 2 == 0)

    print(C - D)


main()


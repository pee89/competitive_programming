def solve(s, n, m):
    if m == 0:
        return s

    res = []
    if n > 0:
        res.extend(s[:n])

    res.extend(sorted(s[n:m + 1], reverse=True))

    if m < len(s) - 1:
        res.extend(s[m + 1:])

    return "".join(res)


if __name__ == "__main__":
    t = int(input())
    for case in range(1, t + 1):
        parts = input().split()
        s, n, m = parts[0], int(parts[1]), int(parts[2])
        result = solve(s, n, m)
        print(result)

defmodule Solution do
  require Integer

  def solve do
    n = IO.read(:stdio, :line) 
        |> String.trim 
        |> String.to_integer
    for _caseno <- 1..n do
      x = IO.read(:stdio, :line) 
          |> String.trim 
          |> String.to_float
      IO.puts ex(x)
    end
  end

  def ex(x) do
    Enum.reduce(1..9, 1, fn(v, acc) -> acc + pow(x, v)/factorial(v) end)
  end

  def factorial(n), do: do_factorial(n, 1)
  defp do_factorial(1, fact), do: fact
  defp do_factorial(n, fact), do: do_factorial(n-1, n*fact)

  def pow(_, 0), do: 1
  def pow(x, n) when Integer.is_odd(n), do: x * pow(x, n-1)
  def pow(x, n) do
    result = pow(x, div(n, 2))
    result * result
  end
end

Solution.solve
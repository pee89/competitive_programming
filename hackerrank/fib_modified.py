def main():
    [a, b, n] = [int(v) for v in input().strip().split()]
    F = [a, b]
    for i in range(2, n):
        F.append(F[i - 1] * F[i - 1] + F[i - 2])
    print(F[n - 1])


if __name__ == '__main__':
    main()

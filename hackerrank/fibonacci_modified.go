package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

func readLines(rdr io.Reader) []string {
	lines := []string{}
	r := bufio.NewReader(rdr)
	for {
		line, err := r.ReadString('\n')
		lines = append(lines, strings.TrimSpace(line))
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
	return lines
}

func readLinesFromFile(filename string) []string {
	f, err := os.Open(filename)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	return readLines(f)
}

func solve(a, b, n int) *big.Int {
	_ = "breakpoint"
	// initialize the array that shall hold the results
	fib := []*big.Int{}
	for i := 0; i < n+1; i++ {
		fib = append(fib, big.NewInt(0))
	}

	// calculate the fib[1] and fib[2]
	fib[1] = big.NewInt(int64(a))
	fib[2] = big.NewInt(int64(b))

	for i := 3; i < n+1; i++ {
		fib[i].Add(big.NewInt(0).Mul(fib[i-1], fib[i-1]), fib[i-2])
	}

	return fib[n]
}

func main() {
	// lines := readLinesFromFile("fibonacci_modified.in")
	lines := readLines(os.Stdin)
	line := lines[0]
	parts := strings.Split(line, " ")
	a, _ := strconv.Atoi(parts[0])
	b, _ := strconv.Atoi(parts[1])
	n, _ := strconv.Atoi(parts[2])
	fmt.Println(solve(a, b, n))
}

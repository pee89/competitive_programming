package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

////////////////////////////////////////////////////////////////////////////////

// INPUT TEMPLATE START

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END

////////////////////////////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("grid_search.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo <= t; caseNo++ {
		RC := mi.readInts()
		R := RC[0]
		C := RC[1]
		G := [][]int{}
		for rowNo := 1; rowNo <= R; rowNo++ {
			rowStr := strings.Split(mi.readLine(), "")
			row := []int{}
			for _, v := range rowStr {
				intVal, _ := strconv.Atoi(v)
				row = append(row, intVal)
			}
			G = append(G, row)
		}
		rc := mi.readInts()
		r := rc[0]
		c := rc[1]
		P := [][]int{}
		for rowNo := 1; rowNo <= r; rowNo++ {
			rowStr := strings.Split(mi.readLine(), "")
			row := []int{}
			for _, v := range rowStr {
				intVal, _ := strconv.Atoi(v)
				row = append(row, intVal)
			}
			P = append(P, row)
		}
		fmt.Println(solve(R, C, G, r, c, P))
	}
}

func solve(R, C int, G [][]int, r, c int, P [][]int) string {
	res := "NO"

	for rowNo, row := range G {
		piAll := partIndexAll(row, P[0], 0)
		if len(piAll) == 0 {
			continue
		}
		// if not enough rows are left in G, return "NO"
		if len(G)-rowNo < len(P) {
			return "NO"
		}
		for _, pi := range piAll {
			match := true
			// fmt.Println("pi", pi)
			for j := 1; j < len(P); j++ {
				if !contains(partIndexAll(G[rowNo+j], P[j], pi), pi) {
					match = false
				}
			}
			if match {
				return "YES"
			}
		}
	}

	return res
}

func partIndexAll(row, part []int, startIndex int) []int {
	res := []int{}

	// fmt.Println("Searching for", part, "in", row, "starting from", startIndex)

	for i := startIndex; i < len(row); i++ {
		if len(row)-i < len(part) {
			// no point in searching the balance
			// fmt.Println("returning res", res)
			return res
		}
		if row[i] == part[0] {
			match := true
			for j := 1; j < len(part); j++ {
				if row[i+j] != part[j] {
					match = false
					break
				}
			}
			if match {
				res = append(res, i)
			}
		}
	}

	// fmt.Println("returning res", res)
	return res
}

func contains(arr []int, val int) bool {
	for _, v := range arr {
		if v == val {
			return true
		}
	}
	return false
}

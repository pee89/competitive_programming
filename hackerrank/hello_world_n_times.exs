defmodule Solution do
  def solve do
    {n, _} = input() |> Integer.parse
    for _ <- 1..n, do: IO.puts "Hello World"
  end

  def input do
    IO.read :stdio, :line
  end    
end

Solution.solve
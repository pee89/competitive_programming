package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// INPUT TEMPLATE

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

// INPUT TEMPLATE END

func main() {
	// f, _ := os.Open("insertion_sort_2.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	size := mi.readInt()
	arr := mi.readInts()

	solve(size, arr)
}

func solve(size int, arr []int) {
	for pos := 1; pos < size; pos++ {
		v := arr[pos]
		for i := 0; i < pos; i++ {
			if arr[i] > v {
				// fmt.Println("i", i, "pos", pos)
				copy(arr[i+1:pos+1], arr[i:pos])
				arr[i] = v
				break
			}
		}
		fmt.Println(strings.Join(covertSliceIntToStr(arr), " "))
	}
}

func covertSliceIntToStr(arr []int) []string {
	res := []string{}
	for _, i := range arr {
		res = append(res, strconv.Itoa(i))
	}
	return res
}

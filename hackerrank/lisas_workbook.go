package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

////////////////////////////////////////////////////////////////////////////////

// INPUT TEMPLATE START

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInt64() int64 {
	line := mi.readLine()
	i, err := strconv.ParseInt(line, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readInt64s() []int64 {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int64{}
	for _, s := range parts {
		tmp, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func (mi *MyInput) readWords() []string {
	line := mi.readLine()
	return strings.Split(line, " ")
}

// INPUT TEMPLATE END

////////////////////////////////////////////////////////////////////////////////

func main() {
	// f, _ := os.Open("lisas_workbook.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	nk := mi.readInts()
	n := nk[0]
	k := nk[1]
	t := mi.readInts()
	fmt.Println(solve(n, k, t))
}

func solve(n, k int, t []int) int {
	t = append([]int{0}, t...)

	pages := [][]int{[]int{}} // a page has the start and end prob nos.
	for cNo := 1; cNo <= n; cNo++ {
		pgReq := t[cNo] / k
		if t[cNo]%k != 0 {
			pgReq += 1
		}
		chProbNo := 0
		for chPgIndx := 1; chPgIndx <= pgReq; chPgIndx++ {
			if chPgIndx < pgReq {
				// all pages except the last page
				pg := []int{chProbNo + 1, chProbNo + k}
				pages = append(pages, pg)
				chProbNo += k
			} else {
				// last page
				pg := []int{chProbNo + 1, t[cNo]}
				pages = append(pages, pg)
			}
		}
	}
	// fmt.Println(pages)

	specialCount := 0
	for pgNo, pg := range pages {
		if pgNo == 0 {
			continue
		}
		if pg[0] <= pgNo && pgNo <= pg[1] {
			specialCount += 1
		}
	}
	return specialCount
}

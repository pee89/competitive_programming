package main

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"strings"
)

func readLines(rdr io.Reader) []string {
	r := bufio.NewReader(rdr)
	lines := []string{}
	for {
		line, err := r.ReadString('\n')
		lines = append(lines, strings.TrimSpace(line))
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
	return lines
}

func readLinesFromFile(filename string) []string {
	f, err := os.Open(filename)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	return readLines(f)
}

func solve(num []int) string {
	_ = "breakpoint"

	negElemPresent := false
	maxEndingAt := []*big.Int{}

	for i, n := range num {
		if n < 0 {
			negElemPresent = true
		}
		if i == 0 {
			maxEndingAt = append(maxEndingAt, big.NewInt(n))
			continue
		}
		var tmp *big.Int
		var withPrev = big.NewInt(x)
		maxEndingAt = append(maxEndingAt)
	}

	return strings.Join(strNum, " ")
}

func main() {
	lines := readLinesFromFile("max_subarray.in")
	// lines := readLines(os.Stdin)
	_ = "breakpoint"
	t, _ := strconv.Atoi(lines[0])
	for caseNo := 1; caseNo <= t; caseNo++ {
		n, _ := strconv.Atoi(lines[2*caseNo-1])
		parts := strings.Split(lines[2*caseNo], " ")
		num := []int{}
		for i := 0; i < n; i++ {
			tmp, _ := strconv.Atoi(parts[i])
			num = append(num, tmp)
		}
		fmt.Println(solve(num))
	}
}

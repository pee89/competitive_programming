import math


def prin(c):
    if c.real and c.imag:
        print('{:.2f} {} {:.2f}i'.format(
            c.real, '+' if c.imag > 0 else '-', abs(c.imag)))
    elif not c.real and c.imag:
        print('{:.2f}i'.format(c.imag))
    elif c.real and not c.imag:
        print('{:.2f}'.format(c.real))
    else:
        print('0.00')


def mod(c):
    return math.sqrt(c.real**2 + c.imag**2)


def solve(fst, snd):
    prin(fst + snd)
    prin(fst - snd)
    prin(fst * snd)
    prin(fst / snd)
    print('{:.2f}'.format(mod(fst)))
    print('{:.2f}'.format(mod(snd)))


if __name__ == '__main__':
    fst = complex(*[float(i) for i in input().split()])
    snd = complex(*[float(i) for i in input().split()])

    solve(fst, snd)

package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type MyInput struct {
	rdr         io.Reader
	lineChan    chan string
	initialized bool
}

func (mi *MyInput) start(done chan struct{}) {
	r := bufio.NewReader(mi.rdr)
	defer func() { close(mi.lineChan) }()
	for {
		line, err := r.ReadString('\n')
		if !mi.initialized {
			mi.initialized = true
			done <- struct{}{}
		}
		mi.lineChan <- strings.TrimSpace(line)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
	}
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if !mi.initialized {
		mi.lineChan = make(chan string)
		done := make(chan struct{})
		go mi.start(done)
		<-done
	}

	res := <-mi.lineChan
	return res
}

func (msc *MyInput) readInt() int {
	line := msc.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (msc *MyInput) readInts() []int {
	line := msc.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func main() {
	// f, _ := os.Open("red_john.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()
	for caseNo := 1; caseNo < t+1; caseNo++ {
		n := mi.readInt()
		fmt.Println(solve(n))
	}
}

func solve(n int) int {
	// TODO
	_ = "breakpoint"

	// count the number of ways, M
	var M int64
	for i := 0; 4*i <= n; i++ {
		t := n - 4*i + i
		r := i
		M += combinations(t, r)
	}
	// fmt.Println(M)

	// find the number of factorials below M i.e. P
	return len(primes(M))
}

// `primes` returns the primes numbers less than or equal to `n`
// using Sieve of Eratosthenes
func primes(N int64) []int64 {
	_ = "breakpoint"

	if N <= 1 {
		return []int64{}
	}

	// create the numbers array
	num := []int64{}
	isPrime := []bool{}
	for i := int64(0); i < N+1; i++ {
		num = append(num, i)
		isPrime = append(isPrime, true)
	}
	isPrime[0] = false
	isPrime[1] = false

	res := []int64{}
	// iterate over the numbers
	// for each number that is prime, mark all its multiples as non-prime
	for i, n := range num {
		if isPrime[i] {
			res = append(res, n)
			for cn := 2 * n; cn < N+1; cn += n {
				isPrime[cn] = false
			}
		}
	}

	return res
}

// FACTORIAL

// `factorial` returns the factorial of `n`
func factorial(n int) int64 {
	res := int64(1)
	for i := int64(2); i <= int64(n); i++ {
		res *= i
	}
	return res
}

// COMBINATIONS

// `combinations` returns the number of combinations for given `n` and `r`
func combinations(n, r int) int64 {
	nr := int64(1)
	for i := int64(n); i > int64(n-r); i-- {
		nr *= i
	}
	dr := factorial(r)
	return nr / dr
}

import sys
sys.setrecursionlimit(10**6)

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()


# ==============================================================


def main():
    t = int(input().strip())

    mod = 10**9 + 7

    from math import factorial
    for _cn in range(1, t + 1):
        n, m = list(map(int, input().strip().split()))
        perms = factorial(n + m - 1) // (factorial(n) * factorial(m - 1))
        print(perms % mod)


main()

# ==============================================================

if debug_mode:
    inf.close()
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type MyInput struct {
	rdr   io.Reader
	lines []string
	index int
}

func (mi *MyInput) readLine() string {
	// if this is the first call, initialize
	if mi.lines == nil {
		r := bufio.NewReader(mi.rdr)
		for {
			line, err := r.ReadString('\n')
			mi.lines = append(mi.lines, strings.TrimSpace(line))
			if err == io.EOF {
				break
			}
			if err != nil {
				panic(err)
			}
		}
	}

	res := mi.lines[mi.index]
	mi.index += 1
	return res
}

func (mi *MyInput) readInt() int {
	line := mi.readLine()
	i, err := strconv.Atoi(line)
	if err != nil {
		panic(err)
	}
	return i
}

func (mi *MyInput) readInts() []int {
	line := mi.readLine()
	parts := strings.Split(line, " ")
	res := []int{}
	for _, s := range parts {
		tmp, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		res = append(res, tmp)
	}
	return res
}

func main() {
	// f, _ := os.Open("stock_maximize.in")
	// mi := MyInput{rdr: f}
	mi := MyInput{rdr: os.Stdin}

	t := mi.readInt()

	for caseNo := 1; caseNo <= t; caseNo++ {
		n := mi.readInt()
		prices := mi.readInts()
		fmt.Println(solve(n, prices))
	}
}

func solve(n int, prices []int) int {
	// if prices are decreasing or equal then profit is 0
	isDecreasing := true
	for i := 1; i < n; i++ {
		if prices[i] > prices[i-1] {
			isDecreasing = false
		}
	}
	if isDecreasing {
		return 0
	}

	// if price today is max of remaining days, sell all
	// else buy 1
	s := 0 // number of shares
	c := 0 // total cost of shares
	e := 0 // earnings by selling shares
	for i := 0; i < n; i++ {
		p := prices[i]

		if i == n-1 || p >= max(prices[i+1:]) {
			// sell all
			e += s * p
			s = 0
		} else {
			c += p
			s += 1
		}
	}
	return e - c
}

func max(prices []int) int {
	var res int
	for _, v := range prices {
		if v > res {
			res = v
		}
	}
	return res
}

use std::io::{stdin, stdout, Write};

struct Scanner<R> {
    reader: R,
    buffer: Vec<String>,
}

impl Default for Scanner<dyn io::BufRead + 'static> {
    fn default() -> Scanner<dyn io::BufRead + 'static> {
        Scanner {
            reader: io::stdin().lock(),
            buffer: vec![],
        }
    }
}

impl<R: io::BufRead> Scanner<R> {
    fn next<T: str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Parse failed");
            }
            let mut input = String::new();
            self.reader.read_line(&mut input).expect("Failed read");
            self.buffer = input.split_whitespace().rev().map(String::from).collect();
        }
    }
}

fn with_scanner() {
    let mut scan = Scanner::default();
    let mut out = stdout();

    let t: usize = scan.next();
    writeln!(out, "{}", t).expect("Not Written");
    for case in 0..t {
        let n: usize = scan.next();
        for _ in 0..n {
            let coords: (i32, i32) = (scan.next(), scan.next());
            writeln!(&mut out, "{}: {:?}", case, coords).expect("Not Written");
        }
    }
}

fn main() {
    // without_scanner();
    with_scanner();
}

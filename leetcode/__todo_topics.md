Python Algorithms: Chapter 6 - Divide, Combine and Conquer

- [ ] divide and conquer
- [ ] mergesort
- [ ] quicksort
- [ ] closest pair of points
- [ ] convex hull
- [ ] greatest slice
- [ ] tree balancing
- [ ] priority queue / binary heap
- [x] partition and select

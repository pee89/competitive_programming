#
# @lc app=leetcode id=218 lang=python3
#
# [218] The Skyline Problem
#
# https://leetcode.com/problems/the-skyline-problem/description/
#
# algorithms
# Hard (31.27%)
# Total Accepted:    87.6K
# Total Submissions: 280.2K
# Testcase Example:  '[[2,9,10],[3,7,15],[5,12,12],[15,20,10],[19,24,8]]'
#
# A city's skyline is the outer contour of the silhouette formed by all the
# buildings in that city when viewed from a distance. Now suppose you are given
# the locations and height of all the buildings as shown on a cityscape photo
# (Figure A), write a program to output the skyline formed by these buildings
# collectively (Figure B).
# ⁠
#
# The geometric information of each building is represented by a triplet of
# integers [Li, Ri, Hi], where Li and Ri are the x coordinates of the left and
# right edge of the ith building, respectively, and Hi is its height. It is
# guaranteed that 0 ≤ Li, Ri ≤ INT_MAX, 0 < Hi ≤ INT_MAX, and Ri - Li > 0. You
# may assume all buildings are perfect rectangles grounded on an absolutely
# flat surface at height 0.
#
# For instance, the dimensions of all buildings in Figure A are recorded as: [
# [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ] .
#
# The output is a list of "key points" (red dots in Figure B) in the format of
# [ [x1,y1], [x2, y2], [x3, y3], ... ] that uniquely defines a skyline. A key
# point is the left endpoint of a horizontal line segment. Note that the last
# key point, where the rightmost building ends, is merely used to mark the
# termination of the skyline, and always has zero height. Also, the ground in
# between any two adjacent buildings should be considered part of the skyline
# contour.
#
# For instance, the skyline in Figure B should be represented as:[ [2 10], [3
# 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ].
#
# Notes:
#
#
# The number of buildings in any input list is guaranteed to be in the range
# [0, 10000].
# The input list is already sorted in ascending order by the left x position
# Li.
# The output list must be sorted by the x position.
# There must be no consecutive horizontal lines of equal height in the output
# skyline. For instance, [...[2 3], [4 5], [7 5], [11 5], [12 7]...] is not
# acceptable; the three lines of height 5 should be merged into one in the
# final output as such: [...[2 3], [4 5], [12 7], ...]
#
#
#
class Solution:
    def getSkyline(self, buildings: List[List[int]]) -> List[List[int]]:
        # using DIVIDE and CONQUER

        # TYPES
        # building: [l: int, r: int, h: int]
        # point: [x: int, h: int]
        # skyline: [points]

        # (skyline) -> skyline
        def remove_redundant(skyline):
            filtered_skyline = []
            for point in skyline:
                if not filtered_skyline:
                    filtered_skyline.append(point)
                    continue
                [_x, h] = point
                [_last_x, last_h] = filtered_skyline[-1]
                if h == last_h:
                    continue
                else:
                    filtered_skyline.append(point)

            return filtered_skyline

        # ([building]) -> [building], [building]
        def divide(buildings):
            n = len(buildings)
            if n <= 1:
                return (buildings, [])
            else:
                return (buildings[:n // 2], buildings[n // 2:])

        # (skyline, skyline) -> skyline
        def combine(left, right):
            combined = []
            hleft, hright = 0, 0

            while left and right:
                lx, lh = left[0]
                rx, rh = right[0]
                x = None

                if lx < rx:
                    x, hleft = lx, lh
                    left.pop(0)

                elif rx < lx:
                    x, hright = rx, rh
                    right.pop(0)

                else:
                    # same x
                    x, hleft, hright = lx, lh, rh
                    left.pop(0)
                    right.pop(0)

                combined.append([x, max(hleft, hright)])

            if left:
                combined.extend(left)
            elif right:
                combined.extend(right)

            filtered = remove_redundant(combined)
            return filtered

        # ([building]) -> skyline
        def divide_and_conquer(buildings):
            if len(buildings) == 0:
                return []

            if len(buildings) == 1:
                return skyline(buildings[0])

            left, right = divide(buildings)
            left_skyline = divide_and_conquer(left)
            right_skyline = divide_and_conquer(right)

            return combine(left_skyline, right_skyline)

        # (building) -> skyline
        def skyline(building):
            [l, r, h] = building
            return [[l, h], [r, 0]]

        # divide and conquer approach to finding the skyline
        # ([building]) -> skyline
        return divide_and_conquer(buildings)

#
# @lc app=leetcode id=96 lang=python3
#
# [96] Unique Binary Search Trees
#
# https://leetcode.com/problems/unique-binary-search-trees/description/
#
# algorithms
# Medium (45.67%)
# Total Accepted:    192.4K
# Total Submissions: 421.3K
# Testcase Example:  '3'
#
# Given n, how many structurally unique BST's (binary search trees) that store
# values 1 ... n?
#
# Example:
#
#
# Input: 3
# Output: 5
# Explanation:
# Given n = 3, there are a total of 5 unique BST's:
#
# ⁠  1         3     3      2      1
# ⁠   \       /     /      / \      \
# ⁠    3     2     1      1   3      2
# ⁠   /     /       \                 \
# ⁠  2     1         2                 3
#
#
#

from typing import Dict


class Solution:
    def numTrees(self, n: int) -> int:
        cache = {i: -1 for i in range(n + 1)}
        return self.countTrees(n, cache)

    def countTrees(self, n: int, cache: Dict[int, int]) -> int:
        if n == 0 or n == 1: return 1

        if cache[n] != -1: return cache[n]

        count = 0
        for i in range(1, n + 1):
            # i is the root node
            # left -> 1..(i-1)
            # right -> (i+1)..n -> 1..(n-i)
            left_trees = self.countTrees(i - 1, cache)
            right_trees = self.countTrees(n - i, cache)
            count += left_trees * right_trees

        cache[n] = count
        return count

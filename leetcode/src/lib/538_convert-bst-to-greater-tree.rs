/*
 * @lc app=leetcode id=538 lang=rust
 *
 * [538] Convert BST to Greater Tree
 *
 * https://leetcode.com/problems/convert-bst-to-greater-tree/description/
 *
 * algorithms
 * Easy (51.63%)
 * Likes:    1368
 * Dislikes: 90
 * Total Accepted:    86.9K
 * Total Submissions: 167.7K
 * Testcase Example:  '[5,2,13]'
 *
 * Given a Binary Search Tree (BST), convert it to a Greater Tree such that
 * every key of the original BST is changed to the original key plus sum of all
 * keys greater than the original key in BST.
 *
 *
 * Example:
 *
 * Input: The root of a Binary Search Tree like this:
 * ⁠             5
 * ⁠           /   \
 * ⁠          2     13
 *
 * Output: The root of a Greater Tree like this:
 * ⁠            18
 * ⁠           /   \
 * ⁠         20     13
 *
 *
 */

// @lc code=start
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    pub fn convert_bst(root: Option<Rc<RefCell<TreeNode>>>) -> Option<Rc<RefCell<TreeNode>>> {
        let (greater_bst, acc) = Self::greater_bst(root, 0);
        greater_bst
    }

    fn greater_bst(
        root: Option<Rc<RefCell<TreeNode>>>,
        acc: i32,
    ) -> (Option<Rc<RefCell<TreeNode>>>, i32) {
        if root.is_none() {
            return (None, acc);
        }
        if let Some(rc) = Rc::try_unwrap(root.unwrap()).ok() {
            let mut node = rc.into_inner();
            let (right, acc) = Self::greater_bst(node.right, acc);
            node.val += acc;
            let (left, acc) = Self::greater_bst(node.left, node.val);
            node.right = right;
            node.left = left;
            (Some(Rc::new(RefCell::new(node))), acc)
        } else {
            (None, acc)
        }
    }
}
// @lc code=end

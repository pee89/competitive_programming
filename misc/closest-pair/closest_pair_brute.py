import sys
from collections import namedtuple


Point = namedtuple('Point', ['x', 'y'])


infinity = float('inf')


if __name__ == '__main__':
    points = []

    fn = sys.argv[1] if len(sys.argv) > 1 else 'data.txt'

    with open(fn) as f:
        N = int(f.readline().strip())

        for i in range(N):
            s = [int(v) for v in f.readline().strip().split()]
            points.append(Point(s[0], s[1]))

    res = (infinity, None, None)
    for p in points:
        for o in points:
            if p == o:
                continue
            y = abs(p.y - o.y)
            x = abs(p.x - o.x)
            d = y * y + x * x
            if d < res[0]:
                res = (d, p, o)

    print(res)

package main

import "fmt"

// ****************************************************************************
// HASH MAP *******************************************************************
// ****************************************************************************

func assertHashMapInterfaceImplementation() {
	var _ MapInterface = (*HashMap)(nil)
}

type HashMap struct {
	m map[interface{}]interface{}
}

// Instantiates a hash map.
func NewHashMap() *HashMap {
	return &HashMap{m: make(map[interface{}]interface{})}
}

// Inserts element into the map.
func (m *HashMap) Put(key interface{}, value interface{}) {
	m.m[key] = value
}

// Searches the elemnt in the map by key and returns its value or nil if key is not found in map.
// Second return parameter is true if key was found, otherwise false.
func (m *HashMap) Get(key interface{}) (value interface{}, found bool) {
	value, found = m.m[key]
	return
}

// Remove the element from the map by key.
func (m *HashMap) Remove(key interface{}) {
	delete(m.m, key)
}

// Returns true if map does not contain any elements
func (m *HashMap) Empty() bool {
	return m.Size() == 0
}

// Returns number of elements in the map.
func (m *HashMap) Size() int {
	return len(m.m)
}

// Returns all keys (random order).
func (m *HashMap) Keys() []interface{} {
	keys := make([]interface{}, m.Size())
	count := 0
	for key, _ := range m.m {
		keys[count] = key
		count += 1
	}
	return keys
}

// Returns all values (random order).
func (m *HashMap) Values() []interface{} {
	values := make([]interface{}, m.Size())
	count := 0
	for _, value := range m.m {
		values[count] = value
		count += 1
	}
	return values
}

// Removes all elements from the map.
func (m *HashMap) Clear() {
	m.m = make(map[interface{}]interface{})
}

func (m *HashMap) String() string {
	str := "HashMap\n"
	str += fmt.Sprintf("%v", m.m)
	return str
}

// ****************************************************************************
// HASH MAP END ***************************************************************
// ****************************************************************************

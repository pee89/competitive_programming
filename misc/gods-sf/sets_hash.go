package main

import (
	"fmt"
	"strings"
)

// ****************************************************************************
// HASH SET *******************************************************************
// ****************************************************************************

func assertHashSetInterfaceImplementation() {
	var _ SetInterface = (*HashSet)(nil)
}

type HashSet struct {
	items map[interface{}]struct{}
}

// Instantiates a new empty set
func NewHashSet() *HashSet {
	return &HashSet{items: make(map[interface{}]struct{})}
}

// Adds the items (one or more) to the set.
func (set *HashSet) Add(items ...interface{}) {
	for _, item := range items {
		set.items[item] = struct{}{}
	}
}

// Removes the items (one or more) from the set.
func (set *HashSet) Remove(items ...interface{}) {
	for _, item := range items {
		delete(set.items, item)
	}
}

// Check if items (one or more) are present in the set.
// All items have to be present in the set for the method to return true.
// Returns true if no arguments are passed at all, i.e. set is always superset of empty set.
func (set *HashSet) Contains(items ...interface{}) bool {
	for _, item := range items {
		if _, contains := set.items[item]; !contains {
			return false
		}
	}
	return true
}

// Returns true if set does not contain any elements.
func (set *HashSet) Empty() bool {
	return set.Size() == 0
}

// Returns number of elements within the set.
func (set *HashSet) Size() int {
	return len(set.items)
}

// Clears all values in the set.
func (set *HashSet) Clear() {
	set.items = make(map[interface{}]struct{})
}

// Returns all items in the set.
func (set *HashSet) Values() []interface{} {
	values := make([]interface{}, set.Size())
	count := 0
	for item, _ := range set.items {
		values[count] = item
		count += 1
	}
	return values
}

func (set *HashSet) String() string {
	str := "HashSet\n"
	items := []string{}
	for k, _ := range set.items {
		items = append(items, fmt.Sprintf("%v", k))
	}
	str += strings.Join(items, ", ")
	return str
}

// ****************************************************************************
// HASH SET END ***************************************************************
// ****************************************************************************

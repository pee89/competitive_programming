package main

import (
	"fmt"
	"strings"
)

// ****************************************************************************
// TREE SET *******************************************************************
// ****************************************************************************

func assertTreeSetInterfaceImplementation() {
	var _ SetInterface = (*TreeSet)(nil)
}

type TreeSet struct {
	tree *RBTree
}

// Instantiates a new empty set with the custom comparator.
func NewTreeSetWith(comparator Comparator) *TreeSet {
	return &TreeSet{tree: NewRBTreeWith(comparator)}
}

// Instantiates a new empty set with the IntComparator, i.e. keys are of type int.
func NewTreeSetWithIntComparator() *TreeSet {
	return &TreeSet{tree: NewRBTreeWithIntComparator()}
}

// Instantiates a new empty set with the StringComparator, i.e. keys are of type string.
func NewTreeSetWithStringComparator() *TreeSet {
	return &TreeSet{tree: NewRBTreeWithStringComparator()}
}

// Adds the items (one or more) to the set.
func (set *TreeSet) Add(items ...interface{}) {
	for _, item := range items {
		set.tree.Put(item, struct{}{})
	}
}

// Removes the items (one or more) from the set.
func (set *TreeSet) Remove(items ...interface{}) {
	for _, item := range items {
		set.tree.Remove(item)
	}
}

// Check wether items (one or more) are present in the set.
// All items have to be present in the set for the method to return true.
// Returns true if no arguments are passed at all, i.e. set is always superset of empty set.
func (set *TreeSet) Contains(items ...interface{}) bool {
	for _, item := range items {
		if _, contains := set.tree.Get(item); !contains {
			return false
		}
	}
	return true
}

// Returns true if set does not contain any elements.
func (set *TreeSet) Empty() bool {
	return set.tree.Size() == 0
}

// Returns number of elements within the set.
func (set *TreeSet) Size() int {
	return set.tree.Size()
}

// Clears all values in the set.
func (set *TreeSet) Clear() {
	set.tree.Clear()
}

// Returns all items in the set.
func (set *TreeSet) Values() []interface{} {
	return set.tree.Keys()
}

func (set *TreeSet) String() string {
	str := "TreeSet\n"
	items := []string{}
	for _, v := range set.tree.Keys() {
		items = append(items, fmt.Sprintf("%v", v))
	}
	str += strings.Join(items, ", ")
	return str
}

// ****************************************************************************
// TREE SET END ***************************************************************
// ****************************************************************************

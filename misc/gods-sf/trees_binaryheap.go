package main

import (
	"fmt"
	"strings"
)

// ****************************************************************************
// BINARY HEAP ****************************************************************
// ****************************************************************************

func assertHeapInterfaceImplementation() {
	var _ TreeInterface = (*Heap)(nil)
}

type Heap struct {
	list       *ArrayList
	Comparator Comparator
}

// Instantiates a new empty heap tree with the custom comparator.
func NewHeapWith(comparator Comparator) *Heap {
	return &Heap{list: NewArrayList(), Comparator: comparator}
}

// Instantiates a new empty heap with the IntComparator, i.e. elements are of type int.
func NewHeapWithIntComparator() *Heap {
	return &Heap{list: NewArrayList(), Comparator: IntComparator}
}

// Instantiates a new empty heap with the StringComparator, i.e. elements are of type string.
func NewHeapWithStringComparator() *Heap {
	return &Heap{list: NewArrayList(), Comparator: StringComparator}
}

// Pushes a value onto the heap and bubbles it up accordingly.
func (heap *Heap) Push(value interface{}) {
	heap.list.Add(value)
	heap.bubbleUp()
}

// Pops (removes) top element on heap and returns it, or nil if heap is empty.
// Second return parameter is true, unless the heap was empty and there was nothing to pop.
func (heap *Heap) Pop() (value interface{}, ok bool) {
	value, ok = heap.list.Get(0)
	if !ok {
		return
	}
	lastIndex := heap.list.Size() - 1
	heap.list.Swap(0, lastIndex)
	heap.list.Remove(lastIndex)
	heap.bubbleDown()
	return
}

// Returns top element on the heap without removing it, or nil if heap is empty.
// Second return parameter is true, unless the heap was empty and there was nothing to peek.
func (heap *Heap) Peek() (value interface{}, ok bool) {
	return heap.list.Get(0)
}

// Returns true if heap does not contain any elements.
func (heap *Heap) Empty() bool {
	return heap.list.Empty()
}

// Returns number of elements within the heap.
func (heap *Heap) Size() int {
	return heap.list.Size()
}

// Removes all elements from the heap.
func (heap *Heap) Clear() {
	heap.list.Clear()
}

// Returns all elements in the heap.
func (heap *Heap) Values() []interface{} {
	return heap.list.Values()
}

func (heap *Heap) String() string {
	str := "BinaryHeap\n"
	values := []string{}
	for _, value := range heap.list.Values() {
		values = append(values, fmt.Sprintf("%v", value))
	}
	str += strings.Join(values, ", ")
	return str
}

// Performs the "bubble down" operation. This is to place the element that is at the
// root of the heap in its correct place so that the heap maintains the min/max-heap order property.
func (heap *Heap) bubbleDown() {
	index := 0
	size := heap.list.Size()
	for leftIndex := index<<1 + 1; leftIndex < size; leftIndex = index<<1 + 1 {
		rightIndex := index<<1 + 2
		smallerIndex := leftIndex
		leftValue, _ := heap.list.Get(leftIndex)
		rightValue, _ := heap.list.Get(rightIndex)
		if rightIndex < size && heap.Comparator(leftValue, rightValue) > 0 {
			smallerIndex = rightIndex
		}
		indexValue, _ := heap.list.Get(index)
		smallerValue, _ := heap.list.Get(smallerIndex)
		if heap.Comparator(indexValue, smallerValue) > 0 {
			heap.list.Swap(index, smallerIndex)
		} else {
			break
		}
		index = smallerIndex
	}
}

// Performs the "bubble up" operation. This is to place a newly inserted
// element (i.e. last element in the list) in its correct place so that
// the heap maintains the min/max-heap order property.
func (heap *Heap) bubbleUp() {
	index := heap.list.Size() - 1
	for parentIndex := (index - 1) >> 1; index > 0; parentIndex = (index - 1) >> 1 {
		indexValue, _ := heap.list.Get(index)
		parentValue, _ := heap.list.Get(parentIndex)
		if heap.Comparator(parentValue, indexValue) <= 0 {
			break
		}
		heap.list.Swap(index, parentIndex)
		index = parentIndex
	}
}

// ****************************************************************************
// BINARY HEAP END ************************************************************
// ****************************************************************************

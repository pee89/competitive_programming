import sys


def solve(data):
    pass


if __name__ == '__main__':

    fn = sys.argv[1] if len(sys.argv) > 0 else 'data.txt'

    with open(fn) as f:
        N = int(f.readline().strip())

        data = [int(i) for i in f.readline().strip().split()]

        res = solve(data)
        print("max sum: {}".format(str(sum(res))))

package main

import (
	"errors"
	"fmt"
	"reflect"
)

func TypeMismatchError(v interface{}, t reflect.Type) error {
	return errors.New(fmt.Sprintf("TypeMismatch: %v is not of type %v", v, t))
}

type Validator func(interface{}) error

func validatorFuncFactory(t reflect.Type) Validator {
	return func(e interface{}) error {
		if reflect.TypeOf(e) != t {
			return TypeMismatchError(e, t)
		} else {
			return nil
		}
	}
}

type Comparator func(a, b interface{}) int

func IntComparator(a, b interface{}) int {
	aInt := a.(int)
	bInt := b.(int)
	switch {
	case aInt < bInt:
		return -1
	case aInt > bInt:
		return 1
	default:
		return 0
	}
}

type Node struct {
	key, value  interface{}
	left, right *Node
	N           int // nodes in subtree rooted here
}

func NewBSTNode(key, value interface{}, n int) *Node {
	return &Node{key: key, value: value, N: n}
}

func (n *Node) String() string {
	if n == nil {
		return "[<nil>]"
	}
	return fmt.Sprintf("[<%d: %s>, left: %s, right, %s]", n.key, n.value, n.left.String(), n.right.String())
}

func (n *Node) hasLeftChild() bool {
	return n.left != nil
}

func (n *Node) hasRightChild() bool {
	return n.right != nil
}

type BST struct {
	root                         *Node
	keyValidator, valueValidator Validator
	comparator                   Comparator
}

func NewBST(keyValidator, valueValidator Validator, comparator Comparator) *BST {
	return &BST{
		keyValidator:   keyValidator,
		valueValidator: valueValidator,
		comparator:     comparator,
	}
}

func (b *BST) String() string {
	return fmt.Sprintf("{%s}", b.root.String())
}

func (b *BST) Size() int {
	return b.size(b.root)
}

func (b *BST) size(n *Node) int {
	if n == nil {
		return 0
	} else {
		return n.N
	}
}

func (b *BST) Get(key interface{}) interface{} {
	if e := b.keyValidator(key); e != nil {
		return nil
	}
	return b.get(b.root, key)
}

func (b *BST) get(r *Node, key interface{}) interface{} {
	if r == nil {
		return nil
	}
	cmp := b.comparator(r.key, key)
	switch {
	case cmp == -1:
		return b.get(r.left, key)
	case cmp == 1:
		return b.get(r.right, key)
	default:
		return r.value
	}
}

func (b *BST) Put(key, value interface{}) error {
	if err := b.keyValidator(key); err != nil {
		return err
	}
	if err := b.valueValidator(value); err != nil {
		return err
	}
	if b.root == nil {
		b.root = NewBSTNode(key, value, 1)
	} else {
		b.put(b.root, key, value)
	}
	return nil
}

func (b *BST) put(n *Node, key, value interface{}) {
	cmp := b.comparator(n.key, key)
	switch {
	case cmp == -1:
		if n.hasRightChild() {
			b.put(n.right, key, value)
		} else {
			n.right = NewBSTNode(key, value, 1)
		}
		n.N++
	case cmp == 1:
		if n.hasLeftChild() {
			b.put(n.left, key, value)
		} else {
			n.left = NewBSTNode(key, value, 1)
		}
		n.N++
	default:
		n.value = value
	}
}

func (b *BST) Min() interface{} {
	if b.root == nil {
		return nil
	}
	return b.min(b.root).key
}

func (b *BST) min(n *Node) *Node {
	if n.left == nil {
		return n
	} else {
		return b.min(n.left)
	}
}

func (b *BST) Max() interface{} {
	if b.root == nil {
		return nil
	}
	return b.max(b.root).key
}

func (b *BST) max(n *Node) *Node {
	if n.right == nil {
		return n
	} else {
		return b.max(n.right)
	}
}

func (b *BST) Floor(key interface{}) interface{} {
	if err := b.keyValidator(key); err != nil {
		return nil
	}
	retNode := b.floor(b.root, key)
	if retNode == nil {
		return nil
	} else {
		return retNode.key
	}
}

func (b *BST) floor(n *Node, key interface{}) *Node {
	if n == nil {
		return nil
	}
	cmp := b.comparator(key, n.key)
	if cmp == 0 {
		return n
	} else if cmp == -1 {
		return b.floor(n.left, key)
	} else {
		t := b.floor(n.right, key)
		if t != nil {
			return t
		} else {
			return n
		}
	}
}

func (b *BST) Ceiling(key interface{}) interface{} {
	if err := b.keyValidator(key); err != nil {
		return nil
	}
	retNode := b.ceiling(b.root, key)
	if retNode == nil {
		return nil
	} else {
		return retNode.key
	}
}

func (b *BST) ceiling(n *Node, key interface{}) *Node {
	if n == nil {
		return nil
	}
	cmp := b.comparator(key, n.key)
	if cmp == 0 {
		return n
	} else if cmp == 1 {
		return b.ceiling(n.right, key)
	} else {
		t := b.ceiling(n.left, key)
		if t != nil {
			return t
		} else {
			return n
		}
	}
}

// Return the node containing key of rank k
func (b *BST) SelectRank(k int) interface{} {
	if b.root == nil || b.root.N < k {
		return nil
	}
	retVal := b.selectRank(b.root, k)
	if retVal == nil {
		return nil
	} else {
		return retVal.key
	}
}

// Return the node containing key of rank k
func (b *BST) selectRank(n *Node, k int) *Node {
	if n == nil {
		return nil
	}
	t := b.size(n.left)
	if t > k {
		return b.selectRank(n.left, k)
	} else if t < k {
		return b.selectRank(n.right, k-t-1)
	} else {
		return n
	}
}

// Return number of keys less than a given key value
func (b *BST) Rank(key interface{}) int {
	if err := b.keyValidator(key); err != nil {
		return -1
	}
	return b.rank(key, b.root)
}

func (b *BST) rank(key interface{}, n *Node) int {
	if n == nil {
		return 0
	}
	cmp := b.comparator(key, n.key)
	if cmp == -1 {
		return b.rank(key, n.left)
	} else if cmp == 1 {
		return 1 + b.size(n.left) + b.rank(key, n.right)
	} else {
		return b.size(n.left)
	}
}

func (b *BST) DeleteMin() {
	b.root = b.deleteMin(b.root)
}

func (b *BST) deleteMin(n *Node) *Node {
	if n == nil {
		return nil
	}
	if n.left == nil {
		return n.right
	}
	n.left = b.deleteMin(n.left)
	n.N = b.size(n.left) + b.size(n.right) + 1
	return n
}

func (b *BST) Delete(key interface{}) {
	if err := b.keyValidator(key); err != nil {
		return
	}
	b.root = b.delete(b.root, key)
}

func (b *BST) delete(n *Node, key interface{}) *Node {
	if n == nil {
		return nil
	}
	cmp := b.comparator(key, n.key)
	if cmp == -1 {
		n.left = b.delete(n.left, key)
	} else if cmp == 1 {
		n.right = b.delete(n.right, key)
	} else {
		if n.left == nil {
			return n.right
		}
		if n.right == nil {
			return n.left
		}
		t := n
		n = b.min(n.right)
		n.right = b.deleteMin(t.right)
		n.left = t.left
	}
	n.N = b.size(n.left) + b.size(n.right) + 1
	return n
}

func (b *BST) Keys() []interface{} {
	return b.KeysRange(b.Min(), b.Max())
}

func (b *BST) KeysRange(lo, hi interface{}) []interface{} {
	queue := make([]interface{}, 0)
	b.keys(b.root, &queue, lo, hi)
	return queue
}

func (b *BST) keys(n *Node, queue *[]interface{}, lo, hi interface{}) {
	if n == nil {
		return
	}
	cmplo := b.comparator(lo, n.key)
	cmphi := b.comparator(hi, n.key)
	if cmplo == -1 {
		b.keys(n.left, queue, lo, hi)
	}
	if cmplo <= 0 && cmphi >= 0 {
		*queue = append(*queue, n.key)
	}
	if cmphi == 1 {
		b.keys(n.right, queue, lo, hi)
	}
}

func main() {
	fmt.Println("Hello, playground")
	intValidator := validatorFuncFactory(reflect.TypeOf(1))
	stringValidator := validatorFuncFactory(reflect.TypeOf(""))
	bst := NewBST(intValidator, stringValidator, IntComparator)

	fmt.Println(bst)
	bst.Put(1, "Hello")
	fmt.Println(bst)
	fmt.Println(bst.root)
	bst.Put(2, "world")
	bst.Put(4, "again")
	bst.Put(3, "test")
	fmt.Println(bst)
	fmt.Println(bst.Size())
	fmt.Println(bst.root.right.N)
	fmt.Println(bst.Min())
	fmt.Println(bst.Floor(6))
	fmt.Println(bst.Ceiling(0))
	fmt.Println(bst.SelectRank(-1))
	fmt.Println(bst.Rank(7))
	bst.Delete(3)
	fmt.Println(bst)
	fmt.Println(bst.Keys())
	fmt.Println(bst.KeysRange(5, 6))
}

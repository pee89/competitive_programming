package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var hasQueen [8][8]bool
var inAttack [8][8]int

func main() {
	// recursive backtracking
	solve(0)
}

func placeQueen(i, j int) {
	// fmt.Println("placing queen at", i, j)
	hasQueen[i][j] = true
	// update attack counts
	// same row
	for c := 0; c < 8; c++ {
		inAttack[i][c]++
	}
	// fmt.Println("after updating row")
	// pp2DIntArr(inAttack)
	// col
	for r := 0; r < 8; r++ {
		inAttack[r][j]++
	}
	// fmt.Println("after updating col")
	// pp2DIntArr(inAttack)
	// diagonals
	var ru, cu int
	if i <= j {
		ru, cu = 0, j-i
	} else if i > j {
		ru, cu = i-j, 0
	}
	// fmt.Println("ru, cu", ru, cu)
	for ; ru < 8 && cu < 8; ru, cu = ru+1, cu+1 {
		inAttack[ru][cu]++
	}
	// fmt.Println("after updating up diagonal")
	// pp2DIntArr(inAttack)
	var rd, cd int
	if i+j < 7 {
		rd, cd = 0, i+j
	} else if i+j == 7 {
		rd, cd = 0, 7
	} else if i+j > 7 {
		rd, cd = i+j-7, 7
	}
	// fmt.Println("rd, cd", rd, cd)
	for ; rd < 8 && cd >= 0; rd, cd = rd+1, cd-1 {
		inAttack[rd][cd]++
	}
	// fmt.Println("after updating down diagonal")
	// pp2DIntArr(inAttack)
	inAttack[i][j] -= 3
	// fmt.Println("finally")
	// pp2DIntArr(inAttack)
}

func removeQueen(i, j int) {
	hasQueen[i][j] = false
	// update attack counts
	// same row
	for c := 0; c < 8; c++ {
		inAttack[i][c]--
	}
	// col
	for r := 0; r < 8; r++ {
		inAttack[r][j]--
	}
	// diagonals
	var ru, cu int
	if i <= j {
		ru, cu = 0, j-i
	} else if i > j {
		ru, cu = i-j, 0
	}
	for ; ru < 8 && cu < 8; ru, cu = ru+1, cu+1 {
		inAttack[ru][cu]--
	}
	var rd, cd int
	if i+j < 7 {
		rd, cd = 0, i+j
	} else if i+j == 7 {
		rd, cd = 0, 7
	} else if i+j > 7 {
		rd, cd = i+j-7, 7
	}
	for ; rd < 8 && cd >= 0; rd, cd = rd+1, cd-1 {
		inAttack[rd][cd]--
	}
	inAttack[i][j] += 3
}

var reader = bufio.NewReader(os.Stdin)

func solve(r int) bool {
	// if this is the 8th queen then we are done
	// fmt.Println("in solve for", r)
	// fmt.Println("at start")
	// pp2DBoolArr(hasQueen)
	// pp2DIntArr(inAttack)
	if r == 7 {
		// can we place the queen somewhere
		for c, cnt := range inAttack[7] {
			if cnt == 0 {
				placeQueen(r, c)
				pp2DBoolArr(hasQueen)
				// pp2DIntArr(inAttack)
				return true
			}
		}
		return false

	} else {
		// r'th row, r'th queen to be placed
		// find the first empty slot
		// place the queen there
		// update the inAttack counts appropritately
		// if there is no empty slot, return false
		for c, cnt := range inAttack[r] {
			if cnt == 0 {
				placeQueen(r, c)
				success := solve(r + 1)
				// fmt.Println("success for", (r + 1), "is", success)
				if !success {
					// next solution is not possible, we need to backtrack
					// _, _ = reader.ReadString('\n')
					// fmt.Println("removing queen at", r, c)
					removeQueen(r, c)
					// fmt.Println("after removing queen")
					// pp2DBoolArr(hasQueen)
					// pp2DIntArr(inAttack)
					continue
				} else {
					return true
				}
			}
		}
	}
	return false
}

// `pp2DStrArr` pretty prints a 2D string array
func pp2DStrArr(data [][]string) {
	for i := 0; i < len(data); i++ {
		fmt.Println(data[i])
	}

}

// `pp2DBoolArr` pretty prints a 2D int array
func pp2DBoolArr(data [8][8]bool) {
	for i := 0; i < 8; i++ {
		row := make([]string, 8)
		for j := 0; j < 8; j++ {
			if data[i][j] {
				row[j] = "Q"
			} else {
				row[j] = "_"
			}
		}
		fmt.Println(strings.Join(row, " "))
	}
}

// `pp2DIntArr` pretty prints a 2D int array
func pp2DIntArr(data [8][8]int) {
	for i := 0; i < 8; i++ {
		fmt.Println(data[i])
	}
}

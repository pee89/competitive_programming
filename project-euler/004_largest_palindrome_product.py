min_3x3 = 10000
max_3x3 = 998001

# find the largest 6 digit palindrome number

# this shall be less than 998001

from math import sqrt

def findAllDivisors(n):
	"""Finds all the divisors of a number n"""
	divisors = []
	x = 1
	while x <= math.sqrt(n):
		if n % x == 0:
			divisors.append(x)
			divisors.append(n/x)
		x += 1
	return list(set(divisors))


def isPalindrome(n):
	"""checks if a number is a palindrome number"""
	str_n = str(n)
	numLen = len(str_n)
	result = True
	for i in range(numLen/2):
		if str_n[i] != str_n[numLen-1-i]:
			result = False
			return result
	return result


def main():
	# find the greatest number less than max_3x3 which can be
	# expressed as a product of two 3 digit nos
	n = max_3x3
	while n > min_3x3:
		print 'checking n > ',n

		if isPalindrome(n):			
			x = 1
			while x<math.sqrt(n):
				if n%x == 0:
					if len(str(x)) == 3 and len(str(n/x)) == 3:
						print "the answer is > ",n," and the factors are ",str(x)," and ",str(n/x)
						return
				x += 1

		n -= 1



if __name__ == '__main__':
	main()
def sum_n(n):
	return n*(n+1)/2

def sum_n_squares(n):
	return n*(n+1)*(2*n+1)/6

def main():
	print sum_n(100)**2 - sum_n_squares(100)

if __name__ == '__main__':
	main()
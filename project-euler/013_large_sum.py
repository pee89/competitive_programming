def main():
	with open('013_input_100_50d_nums','r') as infile:
		sum = 0
		for line in infile:
			sum += long(line)

		first_ten_digits = long(str(sum)[:10])
		print first_ten_digits

if __name__ == '__main__':
	main()
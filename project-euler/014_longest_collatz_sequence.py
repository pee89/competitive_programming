import timeit

def main():
	global seq_lengths
	seq_lengths = {1:1}

	max_n = 1000000

	for n in xrange(1,max_n):
		if n in seq_lengths:
			continue
		else:
			find_collatz_seq_len(n)

	max_length = 0
	max_length_n = 1
	for k,v in seq_lengths.items():
		if k < max_n and v > max_length:
			max_length = v
			max_length_n = k

	print('max collatz length = '+str(max_length)+' for n = '+str(max_length_n))


def find_collatz_seq_len(n):

	if n in seq_lengths:
		return seq_lengths[n]

	if n == 1:
		return 1

	try:
		if n%2 == 0:
			small_len = find_collatz_seq_len(n/2)
			len_collatz_n = 1 + small_len
		else:
			small_len = find_collatz_seq_len(3*n+1)
			len_collatz_n = 1 + small_len

		seq_lengths[n] = len_collatz_n
		return len_collatz_n
	except:
		print('exception caught')
		exit(2)

if __name__ == '__main__':
	print timeit.timeit(main,number=1)
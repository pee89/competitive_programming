import timeit

def main():
	data = []
	with open('067_input_triangle','r') as infile:
		for line in infile:
			data.append([int(n) for n in line.strip().split(' ')])

	N = len(data)
	
	max_t = [[0]*(i+1) for i in range(N)]

	# print max_t

	for x in xrange(N):
		max_t[N-1][x] = data[N-1][x]

	# print max_t

	for r in range(N-2, -1, -1):
		for i in xrange(r+1):
			max_t[r][i] = max(max_t[r+1][i],max_t[r+1][i+1]) + data[r][i]
		# print max_t

	print('maximum total from top to bottom > '+str(max_t[0][0]))

if __name__ == '__main__':
	print(timeit.timeit(main, number=1))
# Breadth-First Search

from collections import deque

from typing import Dict, Optional, Deque
from my_types import Graph, Node, Predecessors


def bfs(G: Graph, start: Node) -> Predecessors:
    P: Dict[Node, Optional[Node]] = {start: None}
    Q: Deque[Node] = deque([start])
    while Q:
        u = Q.popleft()
        for v in G[u]:
            if v in P: continue
            P[v] = u
            Q.append(v)
    return P
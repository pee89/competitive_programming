# Finding Connected Components

from typing import List, Set
from my_types import Graph, Node, Predecessors

from walk import walk


def components(G: Graph) -> List:
    comp = []
    seen: Set = set()  # nodes we have already seen
    for u in G:  # try every starting point
        if u in seen:  # Seen? Ignore it
            continue
        C: Predecessors = walk(G, u)  # traverse component
        seen.update(C)  # add keys of C to seen
        comp.append(list(C.keys()))  # Collect the components
    return comp


# TEST
if __name__ == "__main__":
    from utils import create_graph, add_nodes, add_edge

    graph = create_graph()
    a, b, c, d, e, f = range(6)
    add_nodes(graph, [a, b, c, d, e, f])
    add_edge(graph, a, b)
    add_edge(graph, a, c)
    add_edge(graph, d, e)

    comp = components(graph)
    print(f"comp: {comp}")
    assert len(comp) == 3
    assert [f] in comp
    assert [a, b, c] in comp
    assert [d, e] in comp

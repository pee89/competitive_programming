# General Implemetation of Divide and Conquer Scheme

from typing import List, Callable, Tuple


# S: any
# divide_fn: (any) -> (any, any)
# combine_fn: (any, any) -> any
def divide_and_conquer(
        S: List,
        divide_fn: Callable[[List], Tuple[List, List]],  # function to divide
        combine_fn: Callable[[List, List], List]) -> List:
    if len(S) == 1: return S
    L, R = divide_fn(S)
    A = divide_and_conquer(L, divide_fn, combine_fn)
    B = divide_and_conquer(R, divide_fn, combine_fn)
    return combine_fn(A, B)
# Longest Common Subsequence

# ==============================================================================
# MEMOIZED RECURSIVE

from memo import memo


def rec_lcs(a, b) -> int:
    @memo
    def L(i, j):  # LCS for a[:i], b[:j], i & j inclusive
        if min(i, j) < 0: return 0
        if a[i] == b[j]: return 1 + L(i - 1, j - 1)
        return max(L(i - 1, j), L(i, j - 1))

    return L(len(a) - 1, len(b) - 1)


# ==============================================================================
# ITERATIVE


def lcs(a, b) -> int:
    m, n = len(a), len(b)
    L = [[None for _ in range(n + 1)] for _ in range(m + 1)]

    for r in range(m + 1):
        L[r][0] = 0
    for c in range(n + 1):
        L[0][c] = 0

    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if a[i - 1] == b[j - 1]:
                L[i][j] = 1 + L[i - 1][j - 1]
            else:
                L[i][j] = max(L[i - 1][j], L[i][j - 1])

    return L[m][n]


# ==============================================================================
# TEST

if __name__ == "__main__":
    a = "shashank"
    b = "shank"
    assert rec_lcs(a, b) == 5
    print("recursive passed")

    assert lcs(a, b) == 5
    print("iterative passed")

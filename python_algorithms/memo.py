# A Memoizing Decorator

from functools import wraps


def memo(func):
    cache = {}

    @wraps(func)
    def wrapped(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]

    return wrapped
# Partition and Select
# Select kth smallest item from the sequence in O(n)


# seq: [T] -> (lo: [T], pivot: T, hi: [T])
# when T: any
def partition(seq):
    pivot, seq = seq[0], seq[1:]
    lo = [x for x in seq if x <= pivot]
    hi = [x for x in seq if x > pivot]
    return lo, pivot, hi


# seq: [T], k: int -> T
# when T: any
def select(seq, k):
    lo, pivot, hi = partition(seq)
    m = len(lo)
    if m == k - 1:
        return pivot
    elif m < k:
        return select(hi, k - m - 1)
    else:
        return select(lo, k)


# TEST
if __name__ == "__main__":
    arr = [1, 3, 5, 0, 2]
    assert select(arr, 3) == 2
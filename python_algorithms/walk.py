# Walking through a connected component of a Graph represented using Ajacency Sets

from typing import Set
from my_types import Graph, Node, Predecessors


def walk(G: Graph, s: Node, S: Set[Node]) -> Predecessors:
    P: Predecessors = dict()  # Predecessors
    Q: Set[Node] = set()  # "to do" queue
    P[s] = None  # s has no predecessor
    Q.add(s)  # start with s
    while Q:  # still nodes to visit?
        u = Q.pop()  # pick one arbitrarily
        for v in G[u].difference(P, S):  # New nodes?
            Q.add(v)  # add to "to do" queue
            P[v] = u  # remember where we came from
    return P

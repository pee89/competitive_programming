use std::collections::{HashMap, HashSet};

#[derive(Debug)]
pub struct Graph {
    graph: HashMap<usize, HashSet<usize>>,
}

impl Graph {
    pub fn new() -> Self {
        Self {
            graph: HashMap::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.graph.len() == 0
    }

    pub fn add_node(&mut self, node: usize) {
        self.graph.entry(node).or_default();
    }

    pub fn nodes(&self) -> impl Iterator<Item = usize> + '_ {
        self.graph.keys().cloned()
    }

    pub fn neighbours(&self, node: usize) -> Option<&HashSet<usize>> {
        self.graph.get(&node)
    }

    pub fn add_edge(&mut self, from: usize, to: usize) -> Result<(), String> {
        if !self.has_node(from) {
            return Err(format!("Graph does not have node: {}", from));
        }
        if !self.has_node(to) {
            return Err(format!("Graph does not have node: {}", to));
        }
        self.graph.entry(from).and_modify(|adj| {
            adj.insert(to);
        });
        self.graph.entry(to).and_modify(|adj| {
            adj.insert(from);
        });
        Ok(())
    }

    pub fn has_node(&self, node: usize) -> bool {
        self.graph.contains_key(&node)
    }

    /// returns `()` if edge was remove, or `None` in case no edge was removed
    pub fn remove_edge(&mut self, from: usize, to: usize) -> Option<()> {
        if !self.has_node(from) {
            return None;
        }
        if !self.has_node(to) {
            return None;
        }
        self.graph.entry(from).and_modify(|adj| {
            adj.remove(&to);
        });
        self.graph.entry(to).and_modify(|adj| {
            adj.remove(&from);
        });
        Some(())
    }

    /// returns the node removed, or `None` in case no such node was present
    pub fn remove_node(&mut self, node: usize) -> Option<usize> {
        if !self.has_node(node) {
            return None;
        }

        let neighbours: HashSet<usize> = self.neighbours(node)?.iter().cloned().collect();
        neighbours.iter().for_each(|&target| {
            self.remove_edge(node, target);
        });
        Some(node)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_graph() {
        let g = Graph::new();
        assert!(g.is_empty());
    }

    #[test]
    fn add_node() {
        let mut g = Graph::new();
        for i in 0..10 {
            g.add_node(i)
        }
        assert_eq!(g.nodes().count(), 10);
    }
    #[test]
    fn has_node() {
        let mut g = Graph::new();
        for i in 0..10 {
            g.add_node(i)
        }
        for i in 0..10 {
            assert!(g.has_node(i));
        }
        assert!(!g.has_node(10));
    }

    #[test]
    fn add_edge() {
        let mut g = Graph::new();
        for i in 0..10 {
            g.add_node(i)
        }
        let res = g.add_edge(1, 2);
        println!("{:#?}", res);
        assert!(g.add_edge(1, 2).is_ok());
        println!("{:#?}", g);
        assert!(g.neighbours(1).is_some());
        assert_eq!(g.neighbours(1).unwrap().len(), 1);
        assert!(g.neighbours(1).unwrap().contains(&2));
        assert!(g.neighbours(2).is_some());
        assert_eq!(g.neighbours(2).unwrap().len(), 1);
        assert!(g.neighbours(2).unwrap().contains(&1));
        for i in 3..10 {
            println!("i: {}", i);
            assert_eq!(g.neighbours(i).unwrap().len(), 0);
        }
    }

    #[test]
    fn remove_edge() {
        let mut g = Graph::new();
        for i in 0..10 {
            g.add_node(i);
        }
        let _ = g.add_edge(1, 9);
        let _ = g.add_edge(2, 8);
        let _ = g.add_edge(1, 8);
        g.remove_edge(1, 9);
        assert!(!g.neighbours(1).unwrap().contains(&9));
        assert!(!g.neighbours(9).unwrap().contains(&1));
    }
    #[test]
    fn remove_node() {
        let mut g = Graph::new();
        for i in 0..10 {
            g.add_node(i);
        }
        let _ = g.add_edge(1, 9);
        let _ = g.add_edge(2, 8);
        let _ = g.add_edge(1, 8);
        g.remove_node(8);
        println!("{:#?}", g);
        assert!(g.neighbours(2).unwrap().is_empty());
        assert!(!g.neighbours(1).unwrap().contains(&8));
    }
}

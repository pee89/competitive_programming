if __name__ == '__main__':
    N = int(input().strip())

    for i in range(N):
        x, y = input().strip().split()
        rx, ry = int(x[::-1]), int(y[::-1])
        s = rx + ry
        print(int(str(s)[::-1]))

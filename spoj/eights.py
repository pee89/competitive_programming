def ri():
    return int(input().strip())

if __name__ == '__main__':
    t = ri()
    for i in range(t):
        k = ri()
        pre = k // 4 if k % 4 != 0 else k // 4 - 1
        pre = str(pre)
        # 192 > 442 > 692 > 942
        n = [192, 442, 692, 942][k % 4 - 1]
        n = str(n)
        res = int(pre + n)
        print(res)

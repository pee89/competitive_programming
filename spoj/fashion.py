def main():
    t = int(input().strip())

    for caseno in range(t):
        n = int(input().strip())
        m = [int(i) for i in input().strip().split()]
        w = [int(i) for i in input().strip().split()]

        m.sort()
        w.sort()

        hotness_sum = 0
        for i in range(n):
            hotness_sum += m[i] * w[i]

        print(hotness_sum)


if __name__ == '__main__':
    main()

def Z(n):
    res = 0
    d = 5
    while n >= d:
        res += n // d
        d *= 5
    return res


if __name__ == '__main__':
    N = int(input().strip())

    for caseno in range(1, N + 1):
        n = int(input().strip())
        print(Z(n))

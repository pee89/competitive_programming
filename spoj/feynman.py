if __name__ == '__main__':
    while True:
        n = int(input().strip())
        if n == 0:
            break
        print(int(n * (n + 1) * (2 * n + 1) / 6))

if __name__ == '__main__':
    t = int(input().strip())

    for cn in range(t):
        x, y = input().strip().split()
        x = int(x)
        y = int(y)

        if x == y:
            if x % 2 == 0:
                print(2 * x)
            else:
                print(2 * x - 1)
        elif x - y == 2:
            if x % 2 == 0 and y % 2 == 0:
                print(x + y)
            elif x % 2 != 0 and y % 2 != 0:
                print(x + y - 1)
        else:
            print("No Number")

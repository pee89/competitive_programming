# f = open('5.in')


def ri():
    return int(input().strip())
    # return int(f.readline().strip())


def is_palindrome(s):
    for i in range(len(s) // 2):
        if s[i] != s[len(s) - i - 1]:
            return False
    return True


if __name__ == '__main__':
    t = ri()
    for i in range(t):
        k = ri()
        while True:
            k += 1
            if is_palindrome(str(k)):
                print(k)
                break

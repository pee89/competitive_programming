def is_palindrome(s):
    for i in range(len(s) // 2):
        if s[i] != s[len(s) - i - 1]:
            return False
    return True


# @profile
def main():

    # f = open('5_large.in')

    t = int(input().strip())
    # t = int(f.readline().strip())
    for i in range(t):
        s = input().strip()
        # s = f.readline().strip()

        l = len(s)

        if l == 1:
            v = int(s)
            if v < 9:
                print(v + 1)
                continue

            if v == 9:
                print(11)
                continue

        if is_palindrome(s):
            s = str(int(s) + 1)

        for j in range(l // 2 - 1, -1, -1):
            if s[j] != s[l - j - 1]:
                if s[j] > s[l - j - 1]:
                    s = s[:l - j - 1] + s[j] + s[:j][::-1]
                    print(s)
                    break
                else:
                    end = l // 2 if l % 2 == 0 else l // 2 + 1

                    k = end - 1
                    carry = 1
                    new_trailing = ""
                    while k >= 0 and carry:
                        c = int(s[k])
                        if c == 9:
                            nc = 0
                            carry = 1
                        else:
                            nc = c + carry
                            carry = 0
                        new_trailing = str(nc) + new_trailing
                        k -= 1

                    len_new_trailing = len(new_trailing)
                    st = s[:end - len_new_trailing] + new_trailing
                    s = st + st[:l // 2][::-1]
                    print(s)
                    break

    # f.close()


if __name__ == '__main__':
    main()

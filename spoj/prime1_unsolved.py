from math import sqrt, ceil


def primes_upto_sqrt(n):
    '''returns the list of primes upto sqrt(n)'''
    # if n < 2:
    #     return []

    # sq_n = ceil(sqrt(n))
    # is_prime = [True] * (sq_n + 1)
    # is_prime[0] = False
    # is_prime[1] = False
    # for p in range(2, sq_n):
    #     if is_prime[p]:
    #         i = 2
    #         ip = i * p
    #         while ip <= sq_n:
    #             is_prime[ip] = False
    #             ip += p

    # return (i for (i, v) in enumerate(is_prime) if v)

    return sieve(ceil(sqrt(n)))


def sieve(n):
    '''returns the list of primes upto and including n'''
    if n < 2:
        return []

    is_prime = [True] * (n + 1)
    is_prime[0] = False
    is_prime[1] = False
    for p in range(2, int(sqrt(n) + 1)):
        if is_prime[p]:
            i = 2
            ip = i * p
            while ip <= n:
                is_prime[ip] = False
                ip += p

    return (i for (i, v) in enumerate(is_prime) if v)


if __name__ == '__main__':
    # f = open('2.in')

    N = int(input().strip())
    # N = int(f.readline().strip())

    for case_no in range(N):
        m, n = [int(v) for v in input().strip().split()]
        # m, n = [int(v) for v in f.readline().strip().split()]

        base_primes = list(primes_upto_sqrt(n))
        base_primes_set = set(base_primes)
        res = []

        segments = []
        sq_n = ceil(sqrt(n))
        start = sq_n
        end = 2 * sq_n
        while end <= n:
            segments.append((start, end))
            start += sq_n
            end = min(n + 1, end + sq_n)

        for start, end in segments:
            # TODO
            # use the sieve by construting list of booleans here
            pass

        for i in range(m, n + 1):
            if i in (0, 1):
                continue
            elif i <= base_primes[-1] and i in base_primes_set:
                res.append(i)
            elif all(i % p != 0 for p in base_primes):
                res.append(i)
        print('\n'.join((str(i) for i in res)))
        if case_no != N - 1:
            print('')

    # f.close()

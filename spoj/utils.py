from math import sqrt
from functools import reduce

from functools import wraps


def memo(f):
    cache = {}

    @wraps(f)
    def wrapper(*args):
        if args not in cache:
            cache[args] = f(*args)
        return cache[args]
    return wrapper


def sieve(n):
    '''returns the list of primes upto and including n'''
    if n < 2:
        return []

    is_prime = [True] * (n + 1)
    is_prime[0] = False
    is_prime[1] = False
    for p in range(2, int(sqrt(n) + 1)):
        if is_prime[p]:
            i = 2
            ip = i * p
            while ip <= n:
                is_prime[ip] = False
                ip += p

    return (i for (i, v) in enumerate(is_prime) if v)


def factors(n):
    '''returns the set of all the factors of n'''
    return set(reduce(list.__add__,
                      ([i, n // i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))


def max_power(n, d):
    if n % d != 0:
        return 0

    res = 0
    while n >= d:
        if n % d == 0:
            res += 1
            n /= d
        else:
            break
    return res



if __name__ == '__main__':
    print(max_power(100,2))

package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: beads
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.MessageFormat;

public class beads {
	/** **/

	public static void main(String[] args) throws Exception {
		beads main = new beads();
		/** **/
		// String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		String path = "/Users/surabhi/git/usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		// String a = f.readLine();
		int N = Integer.parseInt(f.readLine());
		String necklace = f.readLine();
		char[] beadsArr = necklace.toCharArray();

		// close the input file after reading
		f.close();

		String result = actualSolve(N, beadsArr);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(int N, char[] beadsArr) {
		System.out.println(beadsArr);

		// SOLVE the data
		String result = "";

		// in case there are only single color beads
		if (!contains(beadsArr, 'r') || !(contains(beadsArr, 'b')))
			return Integer.toString(beadsArr.length);

		int maxBeads = 0;
		for (int i = 0; i < beadsArr.length; i++) {
			int rc = getRightCount(i, beadsArr);
			int maxAllowedLeftBeadIndex = (i + rc) % beadsArr.length;
			int lc = getLeftCount(i, beadsArr, maxAllowedLeftBeadIndex);

			System.out.println(MessageFormat.format(
					"Pos:{0} lc:{1} rc:{2} total:{3}", i, lc, rc, (lc + rc)));

			if (lc + rc > maxBeads) {
				maxBeads = lc + rc;
			}
		}

		result = Integer.toString(maxBeads);
		return result;
	}

	private boolean contains(char[] beadsArr, char c) {
		for (int i = 0; i < beadsArr.length; i++) {
			if (beadsArr[i] == c)
				return true;
		}
		return false;
	}

	private int getRightCount(int i, char[] beadsArr) {
		char firstColor = ' ';
		int chCount = 0;

		// all elements to the right of i including i
		while (true) {
			char colorAtI = beadsArr[i];
			if (firstColor == ' ' && colorAtI != 'w') {
				firstColor = colorAtI;
				chCount++;
			} else if (firstColor == ' ' && colorAtI == 'w') {
				chCount++;
			} else if (firstColor != ' ') {
				if (colorAtI == firstColor || colorAtI == 'w')
					chCount++;
				else
					break;
			}

			// increment i
			i = incrementI(i, beadsArr.length);
		}

		return chCount;
	}

	private int incrementI(int i, int length) {
		if (i == length - 1)
			return 0;
		else
			return i + 1;
	}

	private int getLeftCount(int i, char[] beadsArr, int maxAllowedLeftBeadIndex) {
		char firstColor = ' ';
		int chCount = 0;

		i = decrementI(i, beadsArr.length);

		boolean breakAtNext = false;

		// all elements to the left of i excluding i
		while (true) {
			char colorAtI = beadsArr[i];
			if (firstColor == ' ' && colorAtI != 'w') {
				firstColor = colorAtI;
				chCount++;
			} else if (firstColor == ' ' && colorAtI == 'w') {
				chCount++;
			} else if (firstColor != ' ') {
				if (colorAtI == firstColor || colorAtI == 'w')
					chCount++;
				else
					break;
			}

			if (breakAtNext)
				break;

			// increment i
			i = decrementI(i, beadsArr.length);

			if (i == maxAllowedLeftBeadIndex)
				breakAtNext = true;
		}
		return chCount;
	}

	private int decrementI(int i, int length) {
		if (i == 0)
			return length - 1;
		else
			return i - 1;
	}

}
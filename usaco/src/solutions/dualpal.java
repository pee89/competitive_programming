package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: dualpal
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class dualpal {
	private static final String ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	/** **/

	public static void main(String[] args) throws Exception {
		dualpal main = new dualpal();
		/** **/
		String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		// String path = "/Users/shank/git/usaco/src/solutions/";
		// String path = "/home/shank/git//usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		// String a = f.readLine();
		String input = f.readLine();

		String[] inputParts = input.split(" ");
		int N = Integer.parseInt(inputParts[0]);
		int S = Integer.parseInt(inputParts[1]);

		// close the input file after reading
		f.close();

		String result = actualSolve(N, S);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(int N, int S) {
		System.out.println(MessageFormat.format("N={0} S={1}",
				Integer.toString(N), Integer.toString(S)));

		// SOLVE the data
		String result = "";

		int n = S + 1;
		List<Integer> resultList = new ArrayList<Integer>();

		while (resultList.size() < N) {
			int palCount = 0;
			for (int b = 2; b <= 10; b++) {
				String nInB = Integer.toString(n, b).toUpperCase();
				if (isPalindrome(nInB))
					palCount++;
				if (palCount == 2) {
					resultList.add(n);
					break;
				}
			}
			n++;
		}

		result = join("\n", resultList);

		System.out.println(result);

		return result;
	}

	public static <T> String join(String joinStr, List<T> list) {
		StringBuilder builder = new StringBuilder();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			T t = list.get(i);
			builder.append(t.toString());
			if (i != size-1)
				builder.append(joinStr);
		}

		return builder.toString();
	}

	public static String charFromIntForBase(int n) {
		if (n < 10)
			return String.valueOf(n);
		else {
			return String.valueOf(ALPHABETS.charAt(n - 10));
		}
	}

	public static int intFromCharForBase(String s) {
		try {
			int n = Integer.parseInt(s);
			return n;
		} catch (NumberFormatException nfe) {
			int n = 10 + ALPHABETS.indexOf(s);
			return n;
		}
	}

	public static boolean isPalindrome(String s) {
		int len = s.length();
		for (int i = 0; i < len / 2; i++) {
			if (s.charAt(i) != s.charAt(len - 1 - i)) {
				return false;
			}
		}
		return true;
	}

}
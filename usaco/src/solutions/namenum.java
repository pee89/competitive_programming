package solutions;

/*
 ID: shashan20
 LANG: JAVA
 PROG: namenum
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class namenum {
	/** **/

	public static void main(String[] args) throws Exception {
		namenum main = new namenum();
		/** **/
//		String path = "C:\\Users\\6008321\\git\\usaco\\src\\solutions\\";
		// String path = "/Users/shank/git/usaco/src/solutions/";
		 String path = "/home/shank/git//usaco/src/solutions/";
		// String path = "";
		/** **/
		main.solve(path);
		System.exit(0);
	}

	public void solve(String path) throws Exception {

		String infileName = path + this.getClass().getSimpleName() + ".in";
		String outfileName = path + this.getClass().getSimpleName() + ".out";

		BufferedReader f = new BufferedReader(new FileReader(infileName));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				outfileName)));

		// READ the data
		// String a = f.readLine();
		long n = Long.parseLong(f.readLine());

		// close the input file after reading
		f.close();

		String result = actualSolve(n, path);

		out.write(result + "\n");
		out.close();
		System.out.println("Done..");
	}

	private String actualSolve(long n, String path) throws IOException {
		System.out.println(n);

		// SOLVE the data
		String result = "";

		// prepare the char map
		Map<Integer, String> charMap = new HashMap<Integer, String>();
		charMap.put(2, "ABC");
		charMap.put(3, "DEF");
		charMap.put(4, "GHI");
		charMap.put(5, "JKL");
		charMap.put(6, "MNO");
		charMap.put(7, "PRS");
		charMap.put(8, "TUV");
		charMap.put(9, "WXY");

		// read the data from the dict
		String dictName = path + "dict.txt";
		BufferedReader f = new BufferedReader(new FileReader(dictName));
		String validName = "";
		List<String> validNames = new ArrayList<String>();
		while ((validName = f.readLine()) != null) {
			validNames.add(validName);
		}
		f.close();

		// now solve
		List<String> matchingValidNames = validNames;
		String[] numDigits = Long.toString(n).split("");
		int nDigits = numDigits.length;
		numDigits = Arrays.copyOfRange(numDigits, 1, nDigits);
		for (Iterator<String> iterator = matchingValidNames.iterator(); iterator
				.hasNext();) {
			String string = (String) iterator.next();
			if (string.length() != numDigits.length)
				iterator.remove();
		}
		for (int i = 0; i < numDigits.length; i++) {
			String c = numDigits[i];
			int d = Integer.parseInt(c);
			updateMatchingValidNames(matchingValidNames, d, i, charMap);
		}

		
		System.out.println(matchingValidNames.size());
		if (matchingValidNames.size() != 0)
			result = join("\n", matchingValidNames);
		else
			result = "NONE";

		System.out.println(result);

		return result;
	}

	private void updateMatchingValidNames(List<String> matchingValidNames,
			int d, int i, Map<Integer, String> charMap) {
		String charsForD = charMap.get(d);
		for (Iterator<String> iterator = matchingValidNames.iterator(); iterator
				.hasNext();) {
			String name = (String) iterator.next();
			String charInNameAtI = String.valueOf(name.charAt(i));
			if (charsForD.indexOf(charInNameAtI) == -1) {
				iterator.remove();
			}
		}
	}

	public static <T> String join(String joinStr, List<T> list) {
		StringBuilder builder = new StringBuilder();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			T t = list.get(i);
			builder.append(t.toString());
			if (i != size-1)
				builder.append(joinStr);
		}

		return builder.toString();
	}

}
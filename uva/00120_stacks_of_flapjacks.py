import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    f = open(os.path.abspath(__file__).replace(".py", ".in"))
    inf = f

    def input():
        return f.readline()
else:
    inf = sys.stdin

# ==============================================================


def flip(lst, pos):
    for i in range(pos // 2 + 1):
        lst[i], lst[pos - i] = lst[pos - i], lst[i]


def solve(st):
    res = []
    n = len(st)
    for i in range(n - 1, -1, -1):
        ind = st.index(max(st[:i + 1]))
        if ind == i:
            continue
        elif ind == 0:
            # flip @ i
            flip(st, i)
            res.append(n - i)
        else:
            # move the max elem to the top of the stack by flipping at its location
            flip(st, ind)
            res.append(n - ind)
            # flip @ i
            flip(st, i)
            res.append(n - i)
    res.append(0)
    return res


lines = inf.read().splitlines()
for line in lines:
    st = [int(i) for i in line.split()]
    print(line)
    result = solve(st)
    print(" ".join(str(v) for v in result))

# ==============================================================

if debug_mode:
    f.close()

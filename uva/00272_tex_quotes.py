import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    f = open(os.path.abspath(__file__).replace(".py", ".in"))
    inf = f
    def input():
        return f.readline()
else:
    inf = sys.stdin

# ==============================================================

lines = inf.readlines()
is_opening = True
for line in lines:
    line = line.strip()
    i = 0
    while line.find('"', i) != -1:
        i = line.find('"', i)
        if is_opening:
            line = line[:i] + "``" + line[i+1:]
        else:
            line = line[:i] + "''" + line[i+1:]
        is_opening = not is_opening
        i += 2
    print(line)

# ==============================================================

if debug_mode:
    f.close()

from collections import deque

if __name__ == "__main__":
    first = True

    while True:
        if not first:
            print("")
        else:
            first = False

        n = int(input().strip())
        if n == 0:
            break

        line = input().strip()
        while line != "0":
            # process this input line
            s = []
            op = deque(int(v) for v in line.split())
            for i in range(1, n+1):
                s.append(i)
                try:
                    while s and op.index(s[-1], 0, 1) == 0:
                        s.pop()
                        op.popleft()
                except ValueError:
                    pass

            # print the result
            if s:
                print("No")
            else:
                print("Yes")

            # read the next line
            line = input().strip()

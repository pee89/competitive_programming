if __name__ == "__main__":
    while True:
        try:
            a = [int(v) for v in input().strip().split(" ")]
            n, a = a[0], a[1:]
            if len(a) <= 2:
                print("Jolly")
                continue

            diff = []
            for i in range(1, len(a)):
                diff.append(abs(a[i] - a[i - 1]))

            diff.sort()

            done = False
            for i, v in enumerate(diff):
                if i+1 != v:
                    print("Not jolly")
                    done = True
                    break

            if not done:
                print("Jolly")

        except EOFError:
            break

import sys

debug_mode = True if len(sys.argv) > 1 and sys.argv[1] == "-d" else False
if debug_mode:
    import os
    inf = open(os.path.abspath(__file__).replace(".py", ".in"))

    def input():
        return inf.readline()
else:
    inf = sys.stdin

# ==============================================================

import math
INF = math.inf


# List[int], List[int] -> int
def dist(p1, p2):
    x_diff = abs(p1[0] - p2[0])
    y_diff = abs(p1[1] - p2[1])
    return x_diff * x_diff + y_diff * y_diff


# List[List[int]], List[List[int]], int -> int
def min_crossover(l, r, mx, d):
    l_strip = [point for point in l if mx - point[0] <= d]
    r_strip = [point for point in r if point[0] - mx <= d]
    min_dist = INF
    for p1 in l_strip:
        for p2 in r_strip:
            min_dist = min(min_dist, dist(p1, p2))
    return min_dist


# List[List[int]] -> float | INF
def closest_pair(points):
    if len(points) < 2: return INF
    if len(points) == 2: return dist(points[0], points[1])

    min_x = min(x for [x, y] in points)
    max_x = max(x for [x, y] in points)
    mid_x = (max_x + min_x) // 2
    l = [point for point in points if point[0] <= mid_x]
    r = [point for point in points if point[0] > mid_x]
    lc = closest_pair(l)
    rc = closest_pair(r)
    d = min(lc, rc)
    crossover = min_crossover(l, r, mid_x, d)
    return min(crossover, d)


def main():
    while True:
        n = int(input().strip())
        if n == 0: break
        points = []
        for _ in range(n):
            point = list(map(int, input().strip().split()))
            points.append(point)
        points.sort()
        dsquare = closest_pair(points)
        result = math.sqrt(dsquare)
        if result >= 10000:
            result = "INFINITY"
        else:
            result = "{:.4f}".format(result)
        print(str(result))


main()

# ==============================================================

if debug_mode:
    inf.close()
